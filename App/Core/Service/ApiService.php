<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 11/01/2017
 * Time: 10:41
 */
class ApiService extends Service {
    private
        $user_account_dao = NULL,
        $insurance_dao    = NULL,
        $make_dao         = NULL,
        $model_dao        = NULL;

    public function __construct(ApiContext $context){
        parent::__construct($context);
        $this->user_account_dao = DAO::get(DAO::USER_ACCOUNT_DAO);
        $this->insurance_dao    = DAO::get(DAO::INSURANCE_DAO);
        $this->make_dao         = DAO::get(DAO::MAKE_DAO);
        $this->model_dao        = DAO::get(DAO::MODEL_DAO);
    }

    public function getInsurances(): ArraySet{
        return $this->insurance_dao->findAll();
    }

    public function getMakesByName(string $name): ArraySet{
        return $this->make_dao->findByNameLike($name);
    }

    public function getModelsByName(string $name): ArraySet{
        return $this->model_dao->findByNameLike($name);
    }

    public function getModelsByNameAndMakeId(string $name, int $id): ArraySet{
        return $this->model_dao->findByNameLikeAndMakeId($name, $id);
    }

    public function isValid(string $token_id): bool{
        $token = substr($token_id, 0, 8);
        $id    = substr($token_id, 8);
        if(strlen($token) != 8 || !ctype_digit($id))
            return FALSE;
        $does_exist = $this->user_account_dao->doIdAndTokenMatch($id, $token);
        return (bool) $does_exist;
    }
}