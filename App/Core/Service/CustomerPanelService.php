<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/01/2017
 * Time: 20:33
 */
class CustomerPanelService extends AppService{
    private
        $vehicle_dao   = NULL,
        $insurance_dao = NULL;

    public function __construct(AppContext $app_context){
        parent::__construct($app_context);
        $this->vehicle_dao   = DAO::get(DAO::VEHICLE_DAO);
        $this->insurance_dao = DAO::get(DAO::INSURANCE_DAO);
    }

    public function getInsurances(): ArraySet{
        return $this->insurance_dao->findAll();
    }

    public function renderSepaPDFAsString(Customer $customer): string{
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Assurance Automobile Aixoise');
        $pdf->SetTitle('Mandat de prélèvement SEPA');
        $pdf->SetSubject('Autorisation de prélèvement');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        return $pdf->Output('', 'S');
    }

}