<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 15:58
 */
class RegistrationService extends Service {
    const
        OK                 = 0,
        EMPTY_FIELDS       = 1,
        EMAIL_ALREADY_USED = 2,
        ERRORS_EXIST       = 3,
        EMAIL_NOT_SENT     = 4;
    private
        $customer_dao      = NULL,
        $user_account_dao  = NULL;

    public function __construct(AppContext $context){
        parent::__construct($context);
        $this->customer_dao     = DAO::get(DAO::CUSTOMER_DAO);
        $this->user_account_dao = DAO::get(DAO::USER_ACCOUNT_DAO);
    }

    public function preregister(RegistrationRequest $request, Session $session): int{
        if(PostRequest::isEmpty($request)){
            $session->get('user')->addNotif('Vous devez saisir les champs !');
            return self::EMPTY_FIELDS;
        }
        $errors = $this->checkFields($request);
        if($errors) {
            $session->set("errors", $errors);
            $session->set("data", $request);
            return self::ERRORS_EXIST;
        }
        if(!$this->user_account_dao->isEmailUnique($request->email)) {
            $notif = 'Cette adresse email est déjà utilisée, veuillez en choisir une autre ou contacter un administrateur';
            $session->get('user')->addNotif($notif);
            return self::EMAIL_ALREADY_USED;
        }
        $customer = new Customer;
        $customer->setLastName($request->last_name)
                 ->setFirstName($request->first_name)
                 ->setAddress($request->address)
                 ->setCity($request->city)
                 ->setZipCode($request->zip_code)
                 ->setTelNumber($request->phone_number)
                 ->setIdCard(File::save($request->id_card_tmp_file['tmp_name'], $extension));

        $user_account = new UserAccount($customer);
        $user_account->setEmail($request->email)
                     ->setHash(sha1($request->email.UserAccount::SALT.$request->password));
        $sent = $this->sendValidationMail($request->email , $user_account->getToken());
        if(!$sent) {
            $this->context->getSession()->get('user')->addNotif("Adresse email invalide !");
            return self::EMAIL_NOT_SENT;
        }
        $session->set('awaiting_customer', $customer);
        $session->get('user')->addNotif("Email de validation envoyé !");
        return self::OK;

    }

    public function finalizeRegistration(Session $session): void{
        $customer = $session->get('awaiting_customer');
        $session->unset('awaiting_customer');
        $customer->getUserAccount()->unsetToken();
        $this->customer_dao->record($customer);
        $notif = "Vous venez de valider votre adresse email, votre compte sera actif dès qu'un administrateur l'aura vérifié.";
        $session->get('user')->addNotif($notif);
    }

    private function checkFields(RegistrationRequest $request): stdClass{
        $extensions = ["jpg", "jpeg", "png","JPG", "JPEG", "PNG"];
        $extension = pathinfo($request->id_card_tmp_file['name'], PATHINFO_EXTENSION);
        $errors = (object)[];

        foreach($request as $key => $value)
            if(!is_array($value) && ($value == '' || trim($value == '')))
                $errors->$key = "Champ manquant !";

        if(!ctype_digit($request->zip_code)
        || strlen((string)($request->zip_code)) != 5)
            $errors->zip_code = "Code postal invalide !";

        if(!ctype_digit($request->phone_number)
        || strlen($request->phone_number) != 10)
            $errors->phone_number = "Numero invalide !";

        if(!file_exists($request->id_card_tmp_file['tmp_name'])
        || !is_uploaded_file($request->id_card_tmp_file['tmp_name']))
            $errors->id_card = "Il n'y a aucun fichier !";
        else
            if(!in_array($extension, $extensions))
                $errors->id_card = "Extension non-supportée !";
            else
                if($request->id_card_tmp_file['size'] > 2*MB)
                    $errors->id_card = "L'image dépasse 2MB !";

        if($request->password != $request->password_confirm)
            $errors->password = "Les deux mots de pass ne sont pas identiques !";

        else if($request->email != $request->email_confirm)
            $errors->email = "Les deux adresses mails ne sont pas identiques !";
        return $errors;
    }

    private function sendValidationMail(string $email, string $token): bool{
        $to = $email;
        $link = (object)[];
        $link->url = 'http://'.HTTP_HOST.WEBROOT.'validation/'.urlencode($token).'/';
        $link->title = 'Valider votre compte';
        $subject = "Valider votre email" ;
        $header = "From: noreply@aaa.com" ;
        return (new Mail($to, $header, $subject, 'mail_confirm'))->set('link', $link)->send();
    }
}