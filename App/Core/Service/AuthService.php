<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/12/2016
 * Time: 12:02
 */
use \Doctrine\Common\Util\Debug;

class AuthService extends Service{
    private
        $user_account_dao = NULL;

    public function __construct(AppContext $context){
        parent::__construct($context);
        $this->user_account_dao = DAO::get(DAO::USER_ACCOUNT_DAO);
    }

    public function login(string $email, string $pwd): User{
        $user_account = $this->user_account_dao->findByEmailAndPwd($email, $pwd);

        if(!$user_account){
            $notif = "Identifiants incorrects";
            $user = new Guest;
            $user->addNotif($notif);
            return $user;
        }

        $user = $user_account->getUser();
        if($user instanceof Customer && $assignments = $user->getAssignments()){
            $last_assignment = $user->getLastAssignment();
            if(!$last_assignment->hasBeenRead())
                $user->addNotif($last_assignment->getDescription());
            if(in_array($last_assignment->getStatus()->getId(),[Status::AWAITING, Status::INVALID]))
                $user = new Guest;
            $last_assignment->setRead();

        }
        return $user ?? new Guest;
    }
}