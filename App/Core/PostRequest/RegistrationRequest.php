<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:04
 */

/**
 * @PostRequest
 */
class RegistrationRequest extends LoggingInRequest {
    public
        /**
         * @Field
         */
        $last_name = '',
        /**
         * @Field
         */
        $first_name = '',
        /**
         * @Field(name="pwd2", type="password")
         */
        $password_confirm = '',
        /**
         * @Field
         */
        $address = '',
        /**
         * @Field
         */
        $city = '',
        /**
         * @Field
         */
        $zip_code = '',
        /**
         * @Field
         */
        $phone_number = '',
        /**
         * @Field(name="email2", type="email")
         */
        $email_confirm = '',
        /**
         * @Field(name="id_card", type="file")
         */
        $id_card_tmp_file = [];
}