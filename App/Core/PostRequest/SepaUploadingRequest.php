<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:10
 */

/**
 * @PostRequest
 */
class SepaUploadingRequest extends PostRequest{
    public
        /**
         * @Field(name="sepa", type="file")
         */
        $sepa_tmp_file = [];
}