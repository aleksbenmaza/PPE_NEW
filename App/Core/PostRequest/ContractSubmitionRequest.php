<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 15/01/2017
 * Time: 18:10
 */

/**
 * @PostRequest
 */
class ContractSubmitionRequest extends PostRequest {
    public
        /**
         * @Field(name="insurances")
         */
        $insurance_id = 0,
        /**
         * @Field(name="model_id")
         */
        $model_id = 0,
        /**
         * @Field(type="datetime")
         */
        $purchase_date = NULL,
        /**
         * @Field(name="vin")
         */
        $vin_number = '',
        /**
         * @Field(name="registration")
         */
        $registration_number = '',
        /**
         * @Field(name="card", type="file")
         */
        $registration_card_tmp_file = [];
}