<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/10/2016
 * Time: 19:46
 */
final class AssignmentDAO extends DAO{
    const
        SUBJECT_CLASS=Assignment::class;
    
    protected static
        $fields=[];
    
    public function save(Entity $object): bool{
        // TODO: Implement save() method.
    }

    public function remove(Entity $object): bool{
        // TODO: Implement remove() method.
    }

    public function find(int $limit = 0, bool $persistable = TRUE, Search ...$searchs): ArraySet{
        assert(FALSE);
    }

    public function findByCustomer(Customer $customer, int $limit=0): ArraySet{
        $assigments = new ArraySet(self::SUBJECT_CLASS);
        $select_fields = array_merge([self::ADMIN_FK],self::extractColumnsNames(self::$fields));
        $customer_pk = CustomerDAO::getPrimaryKeys();
        $customer_id = key($customer_pk);
        $customer_pk = current($customer_pk)['field'];
        $search = new Search;

        if ($customer_id) {
            $customer_pv = $customer->$customer_id;
            $search->add($customer_pk, $customer_pv);
        }else
            foreach(CustomerDAO::getFields() as $key => $field)
                if($value = $customer->$key)
                    $search->add($field['field'],$value);

        $query = new Query;
        call_user_func_array([$query,'select'],$select_fields);
        $query->where($search)
              ->from(self::TABLE)
              ->leftJoin(CustomerDAO::TABLE,self::CUSTOMER_FK,'=',$customer_pk)
              ->leftJoin(InsureeDAO::TABLE,self::CUSTOMER_FK,'=',$customer_pk)
              ->leftJoin(PersonDAO::TABLE,self::CUSTOMER_FK,'=',$customer_pk);

        $results=self::$connection->query($query);
        while($result = $results->fetch(PDO::FETCH_ASSOC)){
            $search = new Search;
            $search->add($customer_pk,$result[self::ADMIN_FK]);
            $admin = self::get(self::ADMIN_DAO)->find(1, $search)[0];
            $assigment=new Assignment($customer,$admin);
            $assigments[]=$assigment;

            foreach ($result as $field => $value){
                if($field!=self::ADMIN_FK&&$value){
                    $prop = self::getPropertyName($field);
                    $assigment->$prop = $value;
                }
            }
        } 
        return $assigments;
    }

    public function findByAdmin(Admin $admin, int $limit=0): ArraySet{

    }

    public function findByCustomerAndAdmin(Customer $customer, Admin $admin, int $limit=0): ArraySet{

    }

    public function hydrate(LikesManyAssignments $entity){
        $entity_class = get_class($entity);
        $assignements=NULL;
        switch($entity_class){
            case Customer::class:
                $assignements=$this->findByCustomer($entity);
                break;
            case Admin::class:
                $assignements=$this->findByAdmin($entity);
                break;
        }
        if($assignements===NULL)
            return;
        foreach($assignements as $assignement) {
            $offset = $entity->addAssignment($assignement);
            if($entity->addAssignment($assignement)>=0)
                continue;
            $entity->getAssignments()[-$offset]=$assignement;
        }
    }
}