<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 21/10/2016
 * Time: 16:45
 */
final class ThirdPartyDAO extends DAO{
    const
        SUBJECT_CLASS=ThirdParty::class,
        TABLE='TIERS';

    protected static
        $fields=[];
    
    protected function __construct(){
        parent::__construct();
    }

    public function find(int $limit = 0, bool $persistable = TRUE, Search ...$searchs): ArraySet{
        
        $objects = new ArraySet(self::SUBJECT_CLASS);
        $person_fields=PersonDAO::getFields();
        $insuree_fields=InsureeDAO::getFields();
        $third_party_fields=self::$fields;
        $pk=key($person_fields);
        $fields=array_merge($person_fields,$insuree_fields,$third_party_fields);
        $selected_fields=array_keys($fields);

        $query= new Query;
        call_user_func_array([$query,'select'],$selected_fields);
        $query->from(self::TABLE)
              ->leftJoin(InsureeDAO::TABLE,$pk,'=',$pk)
              ->leftJoin(PersonDAO::TABLE,$pk,'=',$pk);
        call_user_func_array([$query,'where'],$searchs);
        $query->limit($limit);
 
        $query = self::$connection->query($query);
        if(!$query->rowCount())
            return $objects;

        while($results = $query->fetch(PDO::FETCH_ASSOC)) {
            $object = new ThirdParty;
            foreach($results as $key=>$result){
                if(strpos($key,'.'))
                    $key=explode(',',$key)[1];

                $setter=$fields[$key]['setter'];
                $object->$setter($result);
            }
            $objects[]=$object;
        }
        return $objects;
    }

    public function save(Entity $object): bool
    {
        // TODO: Implement save() method.
    }

    public function remove(Entity $object): bool
    {
        // TODO: Implement remove() method.
    }

    protected static function init(){
        InsureeDAO::init();
        $class=new ReflectionClass(self::SUBJECT_CLASS);
        assert($props=$class->getProperties());
        if(apcu_exists(self::SUBJECT_CLASS.'_fields')){
            self::$fields=apcu_fetch(self::SUBJECT_CLASS.'_fields');
            return;
        }
        $fields=[];
        foreach($props as $prop){
            $chars=str_split($prop->getDocComment());
            if(!$chars)
                continue;
            $values=[];
            $buffer=NULL;
            $main_key='';
            foreach($chars as  $char){

                if($char=='@')
                    $buffer='';
                else if($buffer!==NULL && $char!='*')
                    $buffer.=$char;
                else if($buffer!==NULL && $char=='*'){
                    $buffer=explode('=',$buffer);
                    $key=trim($buffer[0]);
                    $value=trim($buffer[1]);
                    if($key=='field')
                        $main_key=$value;
                    else
                        $values[$key]=$value;

                    $buffer=NULL;
                }
            }
            if($values)
                $fields[$main_key]=$values;
        }
        self::$fields=array_diff_key($fields,InsureeDAO::getFields(),PersonDAO::getFields());
        apcu_add(self::SUBJECT_CLASS.'_fields',self::$fields);
    }
}