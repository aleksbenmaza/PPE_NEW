<?php

class ContractDAO extends DAO implements RecorderDAO, RemoverDAO {
    protected const
        SUBJECT_CLASS = Contract::class;

    protected function __construct(){
        parent::__construct();
    }

    public function record(Entity... $entities): void{
        self::$entity_manager->persist($object);
    }

    public function remove(Entity... $entities): void{
        self::$entity_manager->detach($entities);
    }
}