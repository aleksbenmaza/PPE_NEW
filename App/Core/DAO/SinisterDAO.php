<?php

class SinisterDAO extends DAO{
    const 
        SUBJECT_CLASS = Sinister::class,
        TABLE = 'sinistres';
    
    protected static
        $fields=[];
    
    protected function __construct(){
        parent::__construct();
    }
    

    public function create(Entity $object): bool{
        assert($object instanceof Sinistre);

        $query = self::$connection->query("SELECT AUTO_INCREMENT as nextId FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".self::db_name."' AND TABLE_NAME = 'sinistre'");
        $id = $query->fetch()['nextId'];
        $object->set('id_sinistre',$id);
        self::$connection->query('INSERT INTO sinistre(libelle,date_sinistre,heure_sinistre,id_vehicule,code_type,responsabilite, tiers,constat,franchise)
                                  VALUES("'.$object->get('libelle').'","'.$object->get('date_sinistre').'","'.$object->get('heure_sinistre').'",'.$object->get('id_vehicule').','.$object->get('code_type').','.$object->get('responsabilite').','.$object->get('tiers').',"'.$object->get('constat').'",'.$object->get('franchise'));
        return true;
       
    }

    public function save(Entity $object): bool{
        assert($object instanceof Sinistre);
        assert($object->changedFields());
 
        $stm = "UPDATE sinistre SET ";
        foreach($object->changedFields() as &$key) {
            if (is_numeric($object->get($key)))
                $stm .= "$key = $object->get($key),";
            else
                $stm .= "$key = \'$object->get($key)\',";
            unset($key);
        }

        $stm[strlen($stm)-1] = "";
        self::$connection->prepare($stm)
                         ->execute();
        return true;

    }

    public function remove(Entity $object): bool{
        // TODO: Implement delete() method.
    }
} 