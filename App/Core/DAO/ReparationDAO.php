<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/07/2016
 * Time: 18:29
 */
class ReparationDAO extends DAO{
    const 
        SUBJECT_CLASS = Reparation::class,
        TABLE = 'reparations';
    
    protected static
        $fields=[];
    
    protected function __construct(){
        parent::__construct();
    }

    public function save(Entity $object): bool{
        assert($object instanceof Reparation);
        if(!$object->changedFields())
            return FALSE;

            $stm = "UPDATE reparation SET ";
            foreach($object->changedFields() as &$key) {
                if (is_numeric($object->get($key)))
                    $stm .= "$key = $object->get($key),";
                else
                    $stm .= "$key = \'$object->get($key)\',";
                unset($key);
            }

            $stm[strlen($stm)-1] = "";
            self::$connection->prepare($stm)
                             ->execute();
            return true;

    }

    public function create(Entity $object): bool{
        assert($object instanceof Reparation);

        $query = $this->_connexion->query("SELECT AUTO_INCREMENT as nextId FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".self::db_name."' AND TABLE_NAME = 'reparation'");
        $id = $query->fetch()['nextId'];
        $object->set('id_reparation',$id);
        self::$connection->query('INSERT INTO reparation(description,montant,id_sinistre,devis)
                                  VALUES("'.$object->get('description').'",'.$object->get('montant').','.$object->get('id_sinistre').',"'.$object->get('devis').'"');
        return true;

    }

    public function remove(Entity $object): bool{
        // TODO: Implement remove() method.
    }

}