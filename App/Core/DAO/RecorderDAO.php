<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:01
 */
interface RecorderDAO{

    public function record(Entity... $entities): void;
}