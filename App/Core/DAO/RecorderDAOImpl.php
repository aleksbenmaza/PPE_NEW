<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 18/01/2017
 * Time: 11:43
 */
trait RecorderDAOImpl{

    public function record(Entity... $entities): void{
        foreach($entities as $entity)
            assert(get_class($entity) == static::SUBJECT_CLASS);
        self::$entity_manager->persist($entities);
    }
}