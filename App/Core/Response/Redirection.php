<?php

class Redirection extends Response{
	private 
		$url,
		$status;

	public function __construct(? string $url = NULL, int $status = 302){
		$this->url = $url ?? WEBROOT;
		$this->status = $status;
	}

	public function __toString(): string{ 
		header('location: '.$this->url, $this->status);
		return '';
	}
}