<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 06/01/2017
 * Time: 10:01
 */


interface BeforeFilteringController{
    public function beforeFilter(): ? Response;
}