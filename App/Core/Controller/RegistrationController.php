<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/07/2016
 * Time: 00:10
 */

/**
 * @Controller
 */
final class RegistrationController extends AppController implements BeforeFilteringController{
    private
        $registration_service = NULL;
    /**
     * @Action
     * @Route(pattern="inscription/", methods={"GET", "POST"})
     */
    public function showIndex(? LoggingInRequest $input = NULL): View{
        if($this->session->get('data'))
            $data = $this->session->get('data');
        else if($input)
            $data = (array) $input;
        else
            $data = [];
        $title = 'Inscription';
        $errors = NULL;
        if($this->session->get('errors')){
            $errors = $this->session->get('errors');
            $this->session->unset('errors');
        }

        $view = $this->makeView('inscription')->set('head_title', $title)
                                              ->set('head_scripts', ['Public/JavaScript/copy.js']);

        foreach($data as $key => $value)
            $view->set($key , $value);
        if($errors)
            foreach($errors as $key => $error)
                $view->set('error_'.$key,$error);
        return $view;
    }

    /**
     * @Action
     * @Route(pattern="inscription-p/", methods={"POST"})
     */
    public function signUp(RegistrationRequest $input): Redirection{
        $registration_status = $this->registration_service->preregister($input, $this->session);

        switch($registration_status) {
            case RegistrationService::ERRORS_EXIST:
                return new Redirection(WEBROOT . 'inscription/');

            case RegistrationService::EMAIL_ALREADY_USED:
                return new Redirection(WEBROOT . 'inscription/');

            case RegistrationService::EMAIL_NOT_SENT:
                return new Redirection(WEBROOT . 'inscription/');

            case RegistrationService::OK:
                return new Redirection;

            case RegistrationService::EMPTY_FIELDS:
                return new Redirection(WEBROOT.'inscription/');
        }
    }

    /**
     * @Action
     * @Route(pattern="validation/(\w+)/", methods={"GET"})
     */
    public function validate(string $token): Response{
        $bool_1 = !$token;
        $bool_2 = $token;
        $bool_2 = $bool_2 && $this->session->get('awaiting_customer');
        $bool_2 = $bool_2 && $this->session->get('awaiting_customer')->getUserAccount()
                                                                     ->getToken() != $token;

        if($bool_1 || $bool_2)
            return new ErrorView(Response::NOT_FOUND);

        else if(!$this->session->get('awaiting_customer')) {
            $this->user->addNotif("Vous n'avez pas l'autorisation d'acceder cette page.");
            return new Redirection;

        } else{
            $this->registration_service->finalizeRegistration($this->session);
            return new Redirection;
        }
    }

    public function use(Context $context): void{
        parent::use($context);
        $this->registration_service = new RegistrationService($context);
    }

    public function beforeFilter(): ? Response{
        if(!$this->user instanceof Guest) {
            $this->user->addNotif("Vous n\'avez pas l\'autorisation d\'accéder cette page !");
            return new Redirection;
        }
        return NULL;
    }
}