<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 05/01/2017
 * Time: 13:49
 */

/**
 * @Controller
 */
class UserApiController extends Controller implements ContextUsingController{
    protected
        $api_service = NULL;

    /**
     * @Action
     * @Route(pattern="user-api/get-insurances/", methods={"GET"})
     */
    public function getInsurances(): JsonResponse{
        $insurances = $this->api_service->getInsurances();
        return new JsonResponse($insurances);
    }

    public function use(Context $context): void{
        $this->api_service = new ApiService($context);
    }
}