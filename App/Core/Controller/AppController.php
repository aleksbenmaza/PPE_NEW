<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/06/2016
 * Time: 16:27
 */

use \Doctrine\Common\Util\Debug;

abstract class AppController extends Controller implements ContextUsingController{
    protected
        $user          = NULL,
        $session       = NULL,
        $app_service   = NULL;

    public function use(Context $context): void{
        $this->app_service = new AppService($context);
        $this->session = $context->getSession();
        $this->app_service->initUser($this->session);
        $this->user = $this->session->get('user');
    }

    protected function makeView(string $template): View{ 
        $view = new View($template);
        $link1 = new stdClass;
        $link2 = clone $link1;
        if($this->user instanceof Customer) {
            $name = $this->user->getFirstName()." ".$this->user->getLastName()."   ";
            $name = strlen($name) > 20 ?
                substr(($name), -strlen($name),15)."..."
                : $name ;

            $link1->name = $name;
            $link1->url = WEBROOT."espace-assure/";
            $link2->name =  "Deconnexion";
            $link2->url = WEBROOT."deconnexion/";

        } else if($this->user instanceof Admin) {
            $link1->name = "Administration";
            $link1->url = WEBROOT."administration/";
            $link2->name =  "Deconnexion";
            $link2->url = WEBROOT."deconnexion/";

        } else {
            $link2->name = "Connexion/Inscription";
            $link2->url = WEBROOT."authentification/";
            $link1 = NULL;
        }

        $view->set('link1', $link1)
             ->set('link2', $link2)
             ->set('notifs', $this->user->flushNotifs());

        return $view;
    }
}