<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/06/2016
 * Time: 16:59
 */

/**
 * @Controller
 */
class HomeController extends AppController{

    /**
     * @Action
     * @Route(methods={"GET"})
     */
    public function showIndex(): View{
        $title = 'Accueil';
        $view =  $this->makeView('accueil')->set('head_title', $title);
        return $view;
    }
}