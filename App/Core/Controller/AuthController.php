<?php

/**
 * @Controller
 */
final class AuthController extends AppController{
    private
        $auth_service = NULL;

    /**
     * @Action
     * @Route(pattern="authentification/", methods={"GET"})
     */
    public function showIndex(): Response{
        if(!$this->checkPrivileges(__METHOD__))
            return new Redirection(HTTP_REFERER);
        $title = 'Authentification';
        $view  = $this->makeView('connexion')->set('head_title', $title)
                                             //->set('form', new Form(LoggingInRequest::class))
                                             ->set('scripts', ['Public/JavaScript/copy.js']);
        return $view;
    }

    /**
     * @Action
     * @Route(pattern="connexion/", methods={"POST"})
     */
    public function signIn(LoggingInRequest $input): Redirection{
        if(!$this->checkPrivileges(__METHOD__))
            return new Redirection(HTTP_REFERER);
        if($input->email && $input->password){
            $this->user = $this->auth_service->login($input->email, $input->password);
            $this->session->set('user', $this->user); //die(\Doctrine\Common\Util\Debug::dump($this->session));
            if(!$this->user instanceof Guest) {
                if($this->user instanceof Customer && !$this->user->getSepa())
                    $this->user->addNotif("Le document SEPA est manquant !");
               // if(apcu_exists($key = $this->user->getUserAccount()->getToken().$this->user->getId()))
                 //   apcu_store($key, apcu_fetch($key)[] = IP_CLIENT);
                //else
                  //  apcu_add($key, [IP_CLIENT]);
                $redir = new Redirection(WEBROOT.'espace-assure/');
            }else
                $redir = new Redirection(WEBROOT.'authentification/');

        } else
            $redir = new Redirection(WEBROOT.'authentification/');
        return $redir;
    }

    /**
     * @Action
     * @Route(pattern="deconnexion/", methods={"GET"})
     */
    public function signOut(): Redirection{
        if(!$this->checkPrivileges(__METHOD__))
            return new Redirection(HTTP_REFERER);
        $this->session->close();
        if(!HTTP_REFERER || strpos(HTTP_REFERER, 'espace-assure'))
            $redir = new Redirection;
        else
            $redir = new Redirection(HTTP_REFERER);
        return $redir;
    }

    public function use(Context $context): void{
        parent::use($context);
        $this->auth_service = new AuthService($context);
    }

    protected function checkPrivileges(string $action): bool{
        $notif = 'Vous n\'avez pas l\'autorisation d\'accéder cette page !';
        if($this->user instanceof RegisteredUser && $action != 'AuthController::signOut') {
            $this->user->addNotif($notif);
            return FALSE;
        }else if($this->user instanceof Guest && $action == 'AuthController::signOut'){
            $this->user->addNotif($notif);
            return FALSE;
        }else
            return TRUE;
    }
}