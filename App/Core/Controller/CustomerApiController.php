<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 12/10/2016
 * Time: 23:28
 */

/**
 * @Controller
 */
class CustomerApiController extends UserApiController{
    private
        $customer_api_service;

    /**
     * @Action
     * @Route(pattern="customer-api/get-makes/(\w+)/(\w+)/", methods={"GET"})
     */
    public function getMakes(string $token_id, string $name): JsonResponse{
        if(!$this->customer_api_service->isValid($token_id))
            return new JsonResponse(NULL, Response::FORBIDDEN);
        $makes = $this->customer_api_service->getMakesByName($name)->toArray();

        return new JsonResponse($makes);
    }
    /**
     * @Action
     * @Route(pattern="customer-api/get-models/(\w+)/(\w+)/(\d+/)?", methods={"GET"})
     */
    public function getModels(string $token_id, string $name, int $make_id = 0): JsonResponse{
        if(!$this->customer_api_service->isValid($token_id))
            return new JsonResponse(NULL, Response::FORBIDDEN);

        if(!$make_id)
            $models = $this->customer_api_service->getModelsByName($name);
        else
            $models = $this->customer_api_service->getModelsByNameAndMakeId($name, $make_id);

        return new JsonResponse($models);
    }

    public function use(Context $context): void{
        $this->customer_api_service = new ApiService($context);
    }
}