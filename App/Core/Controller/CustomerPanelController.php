<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 10/07/2016
 * Time: 01:54
 */

/**
 * @Controller
 */
class CustomerPanelController extends AppController implements BeforeFilteringController{
    private 
        $contracts              = [],
        $insurances             = [],
        $customer_panel_service = NULL;

    /**
     * @Action
     * @Route(pattern="espace-assure/", methods={"GET"})
     */
    public function showIndex(): View{
        $title = 'Espace personnel';
        $blocks = [];
        if($this->user->getSepa())
            $blocks[] = 'espace-assure-index';
        else
            $blocks[] = 'espace-assure-sepa-form';
        $view = $this->makeView('espace-assure')->set('head_title', $title)
                                                ->set('nom_prenom', $this->user->getFirstName().' '.$this->user->getLastName())
                                                ->set('nb_contrats', count($this->user->getContracts()))
                                                ->set('blocks', $blocks);
        return $view;
    }

    /**
     * @Action
     * @Route(pattern="espace-assure/contrat/(\d+)/", methods={"GET"})
     */
    public function showContract(int $key): View{
        if(!$key XOR !isset($this->user->getContracts()[$key-1]))
            return new ErrorView(Response::NOT_FOUND);

        $c = $this->user->getContracts()[$key-1];
        $blocks = []; 
        
        if(!$c->getContract())
            $blocks[] = 'espace-assure-contrat-form';

        $view = $this->makeView('espace-assure')->set('head_title', 'Contrat n°'.$c->getId())
                                                ->set('blocks', $blocks)
                                                ->set('index', $key-1);
        return $view;
    }

    /**
     * @Action
     * @Route(pattern="nouveau-contrat/", methods={"POST"})
     */
    public function submitContract(ContractSubmitionRequest $input): Redirection{
        die(print_r($input));
        $redirect = new Redirection(WEBROOT.'espace-assure/');
        if(!$this->user->getSepa()) {
            $this->user->addNotif("Le document SEPA est manquant ou invalide, vous ne pouvez pas ajouter de contrat !");
            return $redirect;
        }

        if(!$data){
            $this->user->addNotif("Vous devez remplir le formulaire pour ajouter un contrat !");
            return $redirect;
        }
        $extensions = ["jpg", "jpeg", "png","JPG", "JPEG", "PNG"];
        $extension = pathinfo($data['cg']['name'], PATHINFO_EXTENSION);

        /**
         * @var $input ContractSubmitionRequest
         */
        if(!$input->insurance_id)
            $this->user->addNotif("Vous devez préciser un type de garantie !");
            
        else if(!is_uploaded_file($input->registration_card_tmp_file['tmp_name'] ))
            $this->user->addNotif("Il n\'y a aucun fichier !");

        else if(!$input->registration_number)
            $this->user->addNotif("Vous devez préciser une immatriculation !");

        else if(!Vehicle::hasFormat(Vehicle::REG, $input->registration_number))
            $this->user->addNotif("L'immatriculation n'est pas aux normes (AB-123-CD) !");

        else if(!in_array($extension, $extensions))
            $this->user->addNotif("Extension non-supportée !");

        else if(!$input->model_id)
            $this->user->addNotif("Vous devez spécifier la marque du véhicule !");
            
        else if(!$input->model_id)
            $this->user->addNotif("Vous devez spécifier le modèle du véhicule !");
            
        else if(!$input->purchase_date)
            $this->user->addNotif("Vous devez spécifier la date d'achat !");
            
        else if(($date = explode('/',$data['date_achat']))
            && (count($date)!=3 XOR (strlen($date[0])!=2 && strlen($date[1])!=2 && strlen($date[2])!=4)))
            $this->user->addNotif("La date n'est pas au format JJ/MM/AAAA !");
            
        else if(!$input->vin_number)
            $this->user->addNotif("Vous devez préciser un numéro de série !");
            
        else if(Vehicle::hasFormat(Vehicle::VIN, $input->vin_number))
            $this->user->addNotif("Le numéro de série doit contenir 17 caractères !");
        else {
            $key = $input->insurance_id-1;
            if(!Vehicle::isRegistrationUnique($data['imma'])){
                $this->user->addNotif("Cette immatriculation est déjà utilisée !");
                return $redirect;
            }

            $vehicle = new Vehicle;
            $vehicle->setRegistrationNumber($data['imma']);
            $vehicle->setVIN($data['vin']);
            $vehicle->setInsuree($this->user);

            $insurance = DAO::get(DAO::INSURANCE_DAO)->find(1, (new Search)->add('CODE', $data['code']))[0] ?? NULL;
            $contract = new Contract($insurance, $this->user, $vehicle);
            $contract->persist();
        }
        return $redirect;
    }

    /**
     * @Action
     * @Route(pattern="maj-infos/", methods={"POST"})
     */
    public function updateDetails(DetailsUpdatingRequest $input): Response{

    }

    /**
     * @Action
     * @Route(pattern="upload-sepa/", methods={"POST"})
     */
    public function uploadSepa(SepaUploadingRequest $input): Response{
        $datas = Input::fetchAll();
        if(!(isset($datas['sepa']['tmp_name']) && is_uploaded_file($datas['sepa']['tmp_name'] ))){
            $this->user->addNotif("Il n'y a aucun fichier !");
            return new Redirection('../');
        }
        $ext = pathinfo($datas['sepa']['name'],PATHINFO_EXTENSION);
        $this->user->setSepa(DAO::saveFile($datas['sepa']['tmp_name'],$ext));
        DAO::get(DAO::CUSTOMER_DAO)->save($this->user);
        return new Redirection('../');
    }

    /**
     * @Action
     * @Route(pattern="upload-contrat/(\d+)/", methods={"POST"})
     */
    public function uploadContract(ContractUploadingRequest $input, int $key): Redirection{
        if(!$key XOR !isset($this->user->getContracts()[$key-1]))
            $this->user->addNotif("L'URL n'est pas valide !");
        $datas = Input::fetchAll();
        if(!(isset($datas['contrat']['tmp_name']) && is_uploaded_file($datas['contrat']['tmp_name'] ))){
            $this->user->addNotif("Il n'y a aucun fichier !");
            return new Redirection(WEBROOT.'espace-assure/contrat/'.$key.'/');
        }
        $extension = pathinfo($datas['contrat']['name'],PATHINFO_EXTENSION);
        if(!in_array($extension,["pdf","PDF"])){ 
            $this->user->addNotif("Le fichier doit être au format .pdf !");
            return new Redirection(WEBROOT.'espace-assure/contrat/'.$key.'/');
        }
        
        $c = $this->user->getContrats()[$key-1]; 
        $c->set(CONTRAT,$datas['contrat']['tmp_name'] . '&ext=' . $extension);
        DAO::get(DAO::CONTRACT_DAO)->save($c);
        
        $this->user->addNotif('Contrat mis à jour !');

        return new Redirection(WEBROOT.'espace-assure/contrat/'.$key.'/');
    }

    /**
     * @Action
     * @Route(pattern="nouveau-sinistre/(\d+)/", methods={"POST"})
     */
    public function submitSinister(SinisterSubmitionRequest $input, int $key): Response{
        if(!$key)
            return new ErrorView(Response::NOT_FOUND);
    }

    /**
     * @Action
     * @Route(pattern="nouvelle-reparation/(\d+)/(\d+)/", methods={"POST"})
     */
    public function submitReparation(ReparationSubmitionRequest $input, int $contract_key, int $sinister_key): Response{
        if(!($contract_key && $sinister_key))
            return new ErrorView(Response::NOT_FOUND);

    }

    /**
     * @Action
     * @Route(pattern="telechargement-sepa/", methods={"GET"})
     */
    public function downloadSepa(): Download{
        $str_pdf = $this->customer_panel_service->renderSepaPDFAsString($this->user);
        return new Download($str_pdf, 'filename= sepa_'.$this->user->getId().'.pdf');
        //return new Download($pdf->Output('S'), $pdf->httpencode('filename','sepa_'.$c->getId().'.pdf', FALSE));
    }

    /**
     * @Action
     * @Route(pattern="download-contrat/(\d+)/", methods={"GET"})
     */
    public function downloadContract(int $key): Response{
        if(!$key XOR !isset($this->contracts[$key]))
            return new ErrorView(Response::NOT_FOUND);

        $a = $this->user;
        $c = $a->getContracts()[$key-1];
        $v = $c->getVehicle();
        die('lol');
        $pdf = new FPDI;
        $pdf->AddPage();
        $pdf->setSourceFile(STORAGE_ROOT.'Contrat/CONTRAT-AAA.pdf');
        $tplIdx = $pdf->importPage(1);
        $pdf->useTemplate($tplIdx, 0, 1, 0);
        $pdf->SetFont('Arial');
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(37, 33);
        $pdf->Write(0, $c->getId());
        $pdf->SetXY(38, 43);
        $pdf->Write(0, $a->getId());
        $pdf->SetXY(38, 53);
        $pdf->Write(0, $a->getLastName());
        $pdf->SetXY(38, 58);
        $pdf->Write(0, $a->getFirstName());
        $pdf->SetXY(69, 72);
        $pdf->Write(0, $a->getLastName()." ".$a->getFirstName());
        $pdf->SetXY(16, 82);
        $pdf->Write(0, $v->getRegistration());
        $pdf->SetXY(86, 82);
        $pdf->Write(0, $c->getInsurance()->getCode());
        return new Download($pdf->Output('S'), $pdf->httpencode('filename','contrat_'.$c->get(ID_ASSURANCE).'.pdf',FALSE));
    }

    /**
     * @Action
     * @Route(pattern="espace-assure/contrat/(\d+)/sinistres/", methods={"GET"})
     */
    public function showSinisters(int $key): View{
        if (!$key || !$this->user->getContracts()->containsKey($key - 1))
            return new ErrorView(Response::NOT_FOUND);
        $contract = $this->user->getContracts()[$key - 1];
        $sinisters = [];
        foreach ($contract->getSinisters() as $item) {
            $sinister = $item->toStdClass();
            if ($item instanceof PlainSinister)
                $sinister->type = $item->getType()->getTitle();
            else
                $sinister->type = 'Accident';
            $sinister->checked = (bool)$item->getExpert();
            $sinisters[] = $sinister;
        }

        return $this->makeView('espace-assure')
                    ->set('blocks', ['sinistres-index'])
                    ->set('sinisters', $sinisters)
                    ->set('head_title', 'Sinistres du contrat n°' . $contract->getId());
    }

    public function beforeFilter(): ? Response{
        if (!$this->checkPrivileges())
            return new Redirection;

        if(!$this->user->getUserAccount()->getToken())
            $this->user->getUserAccount()->resetToken();

        foreach ($this->user->getContracts() as $cont)
            $this->contracts[] = ['libelle' => $cont->getId() . ' '
                . substr($cont->getSubsDate(), strlen($cont->getSubsDate()) - 1, 11) . ' '
                . $cont->getVehicle()->getRegistrationNumber(), 'couleur' => 'black'];

        foreach ($this->customer_panel_service->getInsurances() as $insurance)
            $this->insurances[] = $insurance->toStdClass();
        return NULL;
    }

    public function use(Context $context): void{
        Parent::use($context);
        $this->customer_panel_service = new CustomerPanelService($context);
    }

    private function checkPrivileges(): bool{
        if(!($this->user instanceof Customer)) {
            $this->user->addNotif('Vous n\'avez pas l\'autorisation d\'accéder cette page !');
            return FALSE;
            
        }else if(($this->user instanceof Customer
               && $this->user->getLastAssignment()->getStatus()->getId() != Status::VALID)) {
            $this->session->clean();
            return FALSE;

        }else
            return TRUE;
    }

    protected function makeView(string $template): View{
        $view = parent::makeView($template);
        $view->set('scripts', [
                //'Public/JavaScript/jquery-ui.min.js',
                'Public/JavaScript/customer-app.js'
           ])->set('style_sheets', [
                //'Public/StyleSheet/jquery-ui.theme.min.css',
                //'Public/StyleSheet/jquery-ui.structure.min.css'
           ])->set('insurances',$this->insurances)
             ->set('contracts',$this->contracts)
             ->set('js_constants',[
                 'ACCESS_KEY' => '\''.$this->user->getUserAccount()->getToken().
                                      $this->user->getId().'\'',
                 'BASE_URL' => '\''.WEBROOT.'\''
             ]);
        return $view;
    }
}