<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/10/2016
 * Time: 14:01
 */

/**
 * @Entity
 * @Table(name="comptes_utilisateur")
 */
class UserAccount extends Entity{
    const
        SID = 0x64DA,
        SALT = "#^dza2455ç?",
        CODE_AD = 'ADM',
        CODE_EM = 'EMP',
        CODE_CU = 'CLI',
        CODE_EX = 'EXP';
    protected
        /**
         * @Column(unique=true)
         */
        $email,
        /**
         * @Column
         */
        $hash,
        /**
         * @Column
         */
        $token,
        /**
         * @Id
         * @OneToOne(targetEntity="Person", inversedBy="user_account", fetch="EAGER")
         * @JoinColumn(name="id", referencedColumnName="id")
         */
        $user;

    public function __construct(RegisteredUser $user){
        $user->setUserAccount($this);
        $this->user = $user;
        $this->resetToken();
    }

    public function resetToken(): void{
        $this->token = substr(md5(microtime(TRUE)*100000), -8, 8);
    }

    public function unsetToken(): void{
        $this->token = '';
    }
    
    public function getUser(): RegisteredUser{
        return $this->user;
    }

    public function setEmail(string $email): void{
        $this->email = $email;
    }

    public function getEmail(): string{
        return $this->email ?? '';
    }

    public function setHash(string $hash): void{
        $this->hash = $hash;
    }

    public function getHash(): string{
        return $this->hash ?? '';
    }

    public function getToken(): string{
        return $this->token ?? '';
    }
}