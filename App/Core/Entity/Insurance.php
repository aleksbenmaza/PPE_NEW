<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/07/2016
 * Time: 14:43
 */




/** 
 * @Entity
 * @Table(name="types_garantie")
 * @Cache("READ_ONLY")
 */
class Insurance extends Entity implements JsonSerializable{
    const
        SID = 0x7E9B,
        A = 1,
        B = 2,
        C = 3,
        D = 4;
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @Column(name="code") @Unique
         */
        $code = '',
        /**
         * @Column(name="libelle") @Unique
         */
        $title = '',
        /**
         * @Column(name="montant_defaut", type="float")
         */
        $default_amount = 0.0,
        /**
         * @Column(name="franchise_min", type="float")
         */
        $min_deductible = 0.0,
        /**
         * @Column(name="franchise_max", type="float")
         */
        $max_deductible = 0.0,
        /**
         * @OneToMany(targetEntity="Contract", mappedBy="insurance", fetch="LAZY")
         */
        $contracts = NULL,
        /**
         * @ManyToMany(targetEntity="PlainSinisterType", fetch="LAZY", cascade={"ALL"})
         * @JoinTable(
         *     name="types_sinistres_types_garantie",
         *     joinColumns={
         *          @JoinColumn(name="type_garantie_id", referencedColumnName="id")
         *     },
         *     inverseJoinColumns={
         *          @JoinColumn(name="type_sinistres_id", referencedColumnName="id")
         *     }
         * )
         */
        $covered_types = NULL;
    
    public function __construct(){
        $this->contracts     = new ArraySet(Contract::class);
        $this->covered_types = new ArraySet(PlainSinisterType::class);
    }

    public function getId(): int{
        return $this->id;
    }

    public function getCode(): string{
        return $this->code;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title): void{
        $this->title = $title;
    }

    public function getDefaultAmount(){
        return $this->default_amount;
    }

    public function setDefaultAmount($default_amount): void{
        $this->default_amount = $default_amount;
    }

    public function getMinDeductible(){
        return $this->min_deductible;
    }

    public function setMinDeductible($min_deductible): void{
        $this->min_deductible = $min_deductible;
    }

    public function getMaxDeductible(){
        return $this->max_deductible;
    }

    public function setMaxDeductible($max_deductible): void{
        $this->max_deductible = $max_deductible;
    }

    public function getCoveredTypes(): ArrayAccess{
        return $this->covered_types;
    }
    
    public function addSinisterType(PlainSinisterType $plain_sinister_type): bool{
        if($this->covered_types->contains($plain_sinister_type))
            return FALSE;
        $this->covered_types[] = $plain_sinister_type;
        $plain_sinister_type->addInsurance($this);
        return TRUE;
    }

    public function removeSinisterType(PlainSinisterType $coverage): bool{
        if(!$this->contracts->has($coverage))
            return FALSE;
        $index = $this->contracts->index($coverage);
        unset($this->contracts[$index]);
        return TRUE;
    }

    public function getContracts(): ArraySet{
        return self::toArraySet($this->contracts);
    }

    public function addContract(Contract $contract): bool{
        if($this->contracts->contains($contract))
            return FALSE;
        $this->contracts[]=$contract;
        return TRUE;
    }
    
    public function removeContract(Contract $contract): bool{
        if(!$this->contracts->has($contract))
            return FALSE;
        $index = $this->contracts->indexOf($contract);
        unset($this->contracts[$index]);
        return TRUE;
    }
}