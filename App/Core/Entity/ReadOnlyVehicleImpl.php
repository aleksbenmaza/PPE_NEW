<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 12:40
 */
trait ReadOnlyVehicleImpl {
    protected
        /**
         * @Column(name="vin", unique=true)
         */
        $vin,
        /**
         * @Column(name="immatriculation")
         */
        $registration_number,
        /**
         * @Column(name="carte_grise")
         */
        $registration_certificate,
        /**
         * @Column(name="valeur", type="float")
         */
        $value;

    public function getModel(): Model{
        return $this->model;
    }

    public function getSinisters(): ArraySet{
        return self::toArraySet($this->sinisters);
    }

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function getVin(): string{
        return $this->vin ?? '';
    }

    public function getInsuree(): ? Insuree{
        return $this->insuree;
    }

    public function getContract(): ? Contract{
        return $this->contract;
    }

    public function getRegistrationNumber(): string{
        return $this->registration_number ?? '';
    }

    public function getRegistrationCertificate(): string{
        return $this->registration_certificate ?? '';
    }

    public function getValue(): float{
        return $this->value ?? 0.0;
    }
}