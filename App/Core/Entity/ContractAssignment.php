<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/01/2017
 * Time: 11:13
 */

/**
 * @Entity
 */
class ContractAssignment extends Assignment{
    protected
        /**
         * @ManyToOne(targetEntity="Contract", inversedBy="assignments", fetch="EAGER")
         * @JoinColumn(name="contrat_id", referencedColumnName="id", nullable=false)
         */
        $contract = NULL;

    public function __construct(Contract $contract, Admin $admin, Status $status){
        parent::__construct($admin, $status);
        $this->contract = $contract;
    }
}