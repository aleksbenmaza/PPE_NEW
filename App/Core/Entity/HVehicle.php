<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 02/02/2017
 * Time: 12:34
 */

/**
 * @Entity
 * @Table(name="h_vehicules")
 * @Cache("READ_ONLY")
 */
class HVehicle extends HistorizedEntity implements ReadOnlyVehicle {
    use ReadOnlyVehicleImpl;
    protected
        /**
         * @ManyToOne(targetEntity="Model", inversedBy="vehicles", fetch="EAGER")
         * @JoinColumn(name="modele_id", referencedColumnName="id")
         */
        $model = NULL,
        /**
         * @ManyToOne(targetEntity="Insuree", inversedBy="vehicles", fetch="EAGER", cascade={"all"})
         * @JoinColumn(name="assure_id", referencedColumnName="id", nullable=true)
         */
        $insuree = NULL,
        /**
         * @OneToMany(targetEntity="Sinister", mappedBy="vehicle", fetch="LAZY", cascade={"all"})
         */
        $sinisters = NULL,
        /**
         * @OneToOne(targetEntity="HContract", mappedBy="vehicle", fetch="EAGER", cascade={"all"})
         */
        $contract = NULL,
        /**
         * @Id @Column(type="integer")
         */
        $id;
}