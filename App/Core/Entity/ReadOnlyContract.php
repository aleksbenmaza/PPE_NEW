<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 07/02/2017
 * Time: 14:13
 */
interface ReadOnlyContract {

    public function getInsurance(): Insurance;

    public function getVehicle(): Vehicle;

    public function getCustomer(): Customer;

    public function getId(): int;

    public function getAmount(): float;

    public function getSubsDate(): ? DateTime;

    public function getStatus(): int;

    public function getContract(): string;
}