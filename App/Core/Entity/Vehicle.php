<?php

/**
 * @Entity
 * @Table(name="vehicules")
 * @MappedSuperclass
 */

class Vehicle extends Entity implements MutableVehicle {
    use MutableVehicleImpl;
	const
		SID = 0xE138,
        REG = 0,
        VIN = 1;
	protected
		/**
		 * @ManyToOne(targetEntity="Model", inversedBy="vehicles", fetch="EAGER")
		 * @JoinColumn(name="modele_id", referencedColumnName="id")
		 */
		$model,
		/**
		 * @ManyToOne(targetEntity="Insuree", inversedBy="vehicles", fetch="EAGER", cascade={"all"})
         * @JoinColumn(name="assure_id", referencedColumnName="id", nullable=true)
		 */
		$insuree,
		/**
		 * @OneToMany(targetEntity="Sinister", mappedBy="vehicle", fetch="LAZY", cascade={"all"})
		 */
		$sinisters,
        /**
         * @OneToOne(targetEntity="Contract", mappedBy="vehicle", fetch="EAGER", cascade={"all"})
         */
        $contract,
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id;

	public function __construct(Model $model, ? Insuree $insuree){
		$this->model     = $model;
		$model->addVehicle($this);
		$this->insuree   = $insuree;
		$insuree->addVehicles($this);
		$this->sinisters = new ArraySet(Sinister::class);
	}

    public static function hasFormat(int $mode, string $subject) : bool{

	    switch ($mode){
	        case self::REG:
                $number = $subject;
                $number = explode('-', $number);
                if(count($number) == 3 && strlen($number[0]) == 2
                && strlen(($number[0]) . ($number[2])) == 4
                && strlen($number[1]) == 3) {
                    $p0 = str_split($number[0]);
                    $p1 = str_split($number[1]);
                    $p2 = str_split($number[2]);
                    if(count(array_intersect(range('A', 'Z'), $p0)) == 2
                    && count(array_intersect(range('A', 'Z'), $p2)) == 2
                    && count(array_intersect(range('0', '9'), $p1)) == 3)
                        return TRUE;
                }
                return FALSE;

            case self::VIN:
                return strlen($vin) == 17;

            default:
                throw new InvalidArgumentException;
        }
    }
}