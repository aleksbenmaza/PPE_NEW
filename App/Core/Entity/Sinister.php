<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 17/05/2016
 * Time: 08:29
 */

/**
 * @Entity
 * @Table(name="SINISTRES")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMap({
 *     0 = "Accident",
 *     1 = "PlainSinister"
 * })
 */
abstract class Sinister extends Entity implements Assignable {
    use AssignableImpl;
    const
        SID = 0x3BBE,
        ACC = 1,
        VOL = 2,
        INC = 3,
        BRI = 4;

    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @ManyToOne(targetEntity="Vehicle", inversedBy="sinisters", fetch="EAGER")
         * @JoinColumn(name="vehicule_id", referencedColumnName="id")
         */
        $vehicle,
        /**
         * @ManyToOne(targetEntity="Expert", inversedBy="sinisters", fetch="EAGER")
         * @JoinColumn(name="expert_id", referencedColumnName="id", nullable=true)
         */
        $expert,
        /**
         * @OneToOne(targetEntity="Damage", mappedBy="sinister", fetch="EAGER")
         */
        $damage,
        /**
         * @OneToMany(targetEntity="SinisterAssignment", mappedBy="sinister", fetch="LAZY", cascade={"all"})
         */
        $assignments,
        /**
         * @Column(type="date")
         */
        $date,
        /**
         * @Column(name="heure", type="time")
         */
        $time,
        /**
         * @Column(name="libelle")
         */
        $description;
    
    public function __construct(Vehicle $vehicles, ? Expert $expert = NULL){
        $this->vehicle = $vehicle;
        $vehicle->addSinister($this);
        $this->expert  = $expert;
        $expert->addSinister($this);
    }

    public function getId(): int{
        return $this->id;
    }

    public function getVehicle(): Vehicle{
        return $this->vehicle;
    }

    public function setExpert(? Expert $expert): void{
        if($this->expert)
            $this->expert->removeSinister($this);
        $this->expert = $expert;
        $expert->addSinister($this);
    }
    
    public function getExpert(): ? Expert{
        return $this->expert;
    }

    public function setDamage(? Damage $damage): void{
        $this->damage = $damage;
    }

    public function getDamage(): ? Damage{
        return $this->damage;
    }

    public function setDate(DateTime $date): void{
        $this->date = $date;
    }

    public function getDate(): ? DateTime{
        return $this->date;
    }

    public function setTime(DateTime $time): void{
        $this->time = $time;
    }

    public function getTime(): ? DateTime{
        return $this->time;
    }

    public function setDescription(string $description): void{
        $this->description = $description;
    }

    public function getDescription(): string{
        return $this->description;
    }
}