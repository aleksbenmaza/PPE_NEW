<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 12:51
 */
trait ReadOnlyContractImpl{
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @Column(name="montant", type="float")
         */
        $amount,
        /**
         * @Column(name="date_souscription", type="date")
         */
        $subs_date = NULL,
        /**
         * @Column(name="etat", type="integer")
         */
        $status,
        /**
         * @Column(name="contrat")
         */
        $contract;

    public function getInsurance(): Insurance{
        return $this->insurance;
    }

    public function getVehicle(): Vehicle{
        return $this->vehicle;
    }

    public function getCustomer(): Customer{
        return $this->customer;
    }

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function getAmount(): float{
        return $this->amount;
    }

    public function getSubsDate(): ? DateTime{
        return $this->subs_date;
    }

    public function getStatus(): int{
        return $this->status ?? 0;
    }

    public function getContract(): string{
        return $this->contract ?? '';
    }
}