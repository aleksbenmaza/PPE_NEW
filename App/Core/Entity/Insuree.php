<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 17:13
 */

/**
 * @Entity
 * @Table(name="assures")
 */
abstract class Insuree extends Person{
    const
        SID = 0x7F41;
    protected
        /**
         * @OneToMany(targetEntity="Vehicle", mappedBy="insuree", fetch="LAZY", cascade={"all"})
         */
        $vehicles,
        /**
         * @Column(name="bonus_malus", type="float")
         */
        $bonus_malus = 1.0;

    protected function __construct(){
        $this->vehicles    = new ArraySet(Vehicle::class);
        $this->indemnities = new ArraySet(Compensation::class);
    }

    public function setBonusMalus(float $bonus_malus): void{
        $this->bonus_malus = $bonus_malus;
    }

    public function getBonusMalus(): float{
        return $this->bonus_malus ?? 1.0;
    }
    
    public function addVehicles(Vehicle $vehicle): bool{
        return $this->vehicles->add($vehicles);
    }

    public function getVehicles(): ArraySet{
        return self::toArraySet($this->vehicles);
    }
    
    public function removeVehicle(Vehicle $ownership): bool{
        
    }

}