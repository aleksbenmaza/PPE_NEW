<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 25/01/2017
 * Time: 20:40
 */

/**
 * @Entity
 * @Table(name="h_contrats")
 * @Cache("READ_ONLY")
 */
class HContract extends HistorizedEntity implements ReadOnlyContract {
    use ReadOnlyContractImpl;

    protected
        /**
         * @Id @Column(type="integer")
         */
        $id = 0,
        /**
         * @OneToOne(targetEntity="Vehicle", inversedBy="contract", fetch="EAGER")
         * @JoinColumn(name="vehicule_id", referencedColumnName="id")
         */
        $vehicle,
        /**
         * @ManyToOne(targetEntity="Insurance", inversedBy="contracts", fetch="EAGER")
         * @JoinColumn(name="type_garantie_id", referencedColumnName="id")
         */
        $insurance,
        /**
         * @ManyToOne(targetEntity="Customer", inversedBy="contracts", fetch="EAGER")
         * @JoinColumn(name="client_id", referencedColumnName="id")
         */
        $customer,
        /**
         * @OneToMany(targetEntity="ContractAssignment", mappedBy="contract", fetch="LAZY", cascade={"all"})
         */
        $assignments,
        /**
         * @Column(name="montant", type="float")
         */
        $amount = 0.0,
        /**
         * @Column(name="date_souscription", type="date")
         */
        $subs_date = NULL,
        /**
         * @Column(name="etat", type="integer")
         */
        $status = 0;
}