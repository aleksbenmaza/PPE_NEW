<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 19/10/2016
 * Time: 19:21
 */

/**
 * @Entity
 * @Table(name="affectations")
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMap({
 *      1="CustomerAssignment",
 *      2="ContractAssignment",
 *      3="SinisterAssignment"
 * })
 */
abstract class Assignment extends Entity{
    const
        SID = 0xB1A1;
    protected
        /**
         * @Id @Column(name="id", type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @ManyToOne(targetEntity="Admin", inversedBy="assignments", fetch="EAGER")
         * @JoinColumn(name="admin_id", referencedColumnName="id", nullable=false)
         */
        $admin = NULL,
        /**
         * @ManyToOne(targetEntity="Status", inversedBy="assignments", fetch="EAGER")
         * @JoinColumn(name="statut_id", referencedColumnName="id", nullable=false)
         */
        $status = NULL,
        /**
         * @Column(name="recu", type="boolean")
         */
        $read = FALSE;

    public function __construct(Admin $admin, Status $status){
        $this->admin  = $admin;
        $this->status = $status;
    }

    public function getAdmin(): Admin{
        return $this->admin;
    }

    public function getDateTime(): string{
        return $this->date_time;
    }
    
    public function setStatus(string $status): Assignment{
        $this->status = $status;
        return $this;
    }
    
    public function getStatus(): Status{
        return $this->status;
    }
    
    public function getDescription(): string{
        return $this->status->getDescription();
    }
    
    public function setRead(bool $read = TRUE){
        $this->read = $read;
    }
    
    public function hasBeenRead(): bool{
        return $this->read;
    }
}