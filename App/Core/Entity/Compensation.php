<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 21:08
 */

/**
 * @Entity
 * @Table(name="indemnisations")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="type", type="integer")
 * @DiscriminatorMapping({
 *     0="CustomerCompensation",
 *     1="CompanyCompensation"
 * })
 */
abstract class Compensation extends Entity{
    const
        SID = 0xB003;
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @Column(name="montant", type="float")
         */
        $amount = 0.0,
        /**
         * @Column(name="franchise", type="float")
         */
        $deductible = 0.0;

    public function getDeductible(): float{
        return $this->deductible ?? '';
    }

    public function setDeductible(float $deductible): void{
        $this->deductible = $deductible;
    }

    public function getAmount(): float{
        return $this->amount ?? 0.0;
    }

    public function setAmount(float $amount): void{
        $this->amount = $amount;
    }

    public function getId(): int{
        return $this->id ?? 0;
    }
}