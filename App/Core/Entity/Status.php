<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/11/2016
 * Time: 17:35
 */

/**
 * @Entity
 * @Table(name="statuts")
 * @Cache("READ_ONLY")
 */
class Status extends Entity{
    const
        SID = 0x99A2,
        VALID = 2,
        INVALID = 3,
        AWAITING = 1;
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @Column
         */
        $description,
        /**
         * @OneToMany(targetEntity="Assignment", mappedBy="status", fetch="LAZY", cascade={"all"})
         */
        $assignments;

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function getDescription(): string{
        return $this->description ?? '';
    }
}