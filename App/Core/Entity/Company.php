<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 15:13
 */

/**
 * @Entity
 * @Table(name="compagnies")
 * @Cache("READ_ONLY")
 */
class Company extends Entity implements Compensable{
    use CompensableImpl;
    protected
        /**
         * @OneToMany(targetEntity="ThirdParty", mappedBy="company", fetch="LAZY", cascade={"ALL"})
         */
        $insurees,
        /**
         * @Id @Column @GeneratedValue
         */
        $id,
        /**
         * @Column(name = "nom")
         */
        $name,
        /**
         * @OneToMany(targetEntity="CompanyCompensation", mappedBy="compensable", fetch="LAZY", cascade={"ALL"})
         */
        $compensations;

    public function __construct(){
        $this->compensations = new ArraySet(Compensation::class);
        $this->insurees = new ArraySet(ThirdParty::class);
    }

    public function getInsurees(): ArraySet{
        return self::toArraySet($this->insurees);
    }

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function getName(): string{
        return $this->name ?? '';
    }
}