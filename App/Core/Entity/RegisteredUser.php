<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 05/01/2017
 * Time: 16:30
 */
interface RegisteredUser extends User{

    public function getUserAccount(): ? UserAccount;

    public function setUserAccount(? UserAccount $user_account): void;

    public function __sleep(): array;
}