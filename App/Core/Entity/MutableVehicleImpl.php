<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 12:40
 */
trait MutableVehicleImpl {
    use ReadOnlyVehicleImpl;

    public function addSinister(Sinister $sinister): bool{
        if($this->sinisters->contains($sinister))
            return FALSE;
        $this->sinisters[] = $sinister;
        return TRUE;
    }

    public function removeSinister(Sinister $sinister): bool{
        return $this->sinisters->removeElement($sinister);
    }

    public function setVIN(string $vin): void{
        $this->vin = $vin;
    }

    public function setInsuree(? Insuree $insuree): void{
        $this->insuree = $insuree;
    }

    public function setContract(? Contract $contract): void{
        $this->contract = $contract;
    }

    public function setRegistrationNumber(int $registration_number): void{
        $this->registration_number = $registration_number;
    }
}