<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 17:28
 */

/**
 * @Entity
 * @Table(name="administrateurs")
 */
class Admin extends Person {
    use RegisteredUserImpl;
    const
        SID = 0x957C;
    protected
        /**
         * @Column(name="role")
         */
        $role = '',
        /**
         * @OneToMany(targetEntity="Assignment", mappedBy="admin", fetch="LAZY", cascade={"all"})
         */
        $assignments;

    public function __construct(){
        $this->assignments = new ArraySet(Assignment::class);
    }

    public function setRole(string $role): void{
        $this->role = $role;
    }

    public function getRole(): string{
        return $this->role;
    }

    /**
     * If there's no duplicate, method returns the new offset,
     * If not, returns the negative value of the existing offset
     */

    public function getAssignments(): ArraySet{
        return self::toArraySet($this->assignments);
    }

    public function addAssignment(Assignment $assignment): bool{
        return $this->assignments->add($assignment);
    }

    public function removeAssignment(Assignment $assignment): bool{
        return $this->assignments->remove($assignment);
    }
}