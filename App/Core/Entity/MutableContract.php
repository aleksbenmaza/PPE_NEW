<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 07/02/2017
 * Time: 14:11
 */
interface MutableContract extends ReadOnlyContract{

    public function setAmount(float $amount);

    public function setSubsDate(DateTime $subs_date): void;

    public function setStatus(int $status): void;

    public function setContract(string $contract): void;
}