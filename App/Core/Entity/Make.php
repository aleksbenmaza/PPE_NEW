<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 13/10/2016
 * Time: 00:02
 */

/**
 * @Entity
 * @Table(name="marques")
 * @Cache("READ_ONLY")
 */
class Make extends Entity{
    const
        SID = 0xCCA0;
    protected
        /**
         * @OneToMany(targetEntity="Model", mappedBy="make", fetch="LAZY")
         */
        $models,
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @Column(name="nom")
         */
        $name = '';

    public function __construct(){
        $this->models = new ArraySet(Model::class);
    }

    public function getModels(): ArrayAccess{
        return clone $this->models;
    }
    
    public function addModel(Model $model): bool{
        return $this->models->add($model);
    }

    public function removeModel(Model $model): bool{
        return $this->models->removeElement($model);
    }
    
    public function getId(): int{
        return $this->id;
    }
    
    public function setName(string $name): void{
        $this->name = $name;
    }
    
    public function getName(): string{
        return $this->name;
    }
}