<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 24/01/2017
 * Time: 11:43
 */
trait AssignableImpl{

    public function getAssignments(): ArraySet{
        return self::toArraySet($this->assignments);
    }

    public function addAssignment(Assignment $assignment): bool{
        return $this->assignments->add($assignment);
    }

    public function removeAssignment(Assignment $assignment): bool{
        return $this->assignments->removeElement($assignment);
    }

    public function getLastAssignment(): Assignment{
        $last = $this->assignments->count()-1;
        return $this->assignments[$last];
    }
}