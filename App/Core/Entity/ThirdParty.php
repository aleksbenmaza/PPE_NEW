<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 17:26
 */

/**
 * @Entity
 * @Table(name="tiers")
 */
class ThirdParty extends Insuree{
    const
        SID = 0x673E;
    protected
        /**
         * @ManyToOne(targetEntity="Company", inversedBy="insurees", fetch="EAGER")
         * @JoinColumn(name="compagnie_id", referencedColumnName="id")
         */
        $company;

    public function __construct(Company $company){
        parent::__construct();
        $this->company = $company;
    }

    public function getCompagny(): Company{
        return $this->company;
    }
}