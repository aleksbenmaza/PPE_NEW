<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 07/02/2017
 * Time: 14:10
 */
interface MutableVehicle extends ReadOnlyVehicle {

    public function removeSinister(Sinister $sinister): bool;

    public function setVIN(string $vin): void;

    public function setInsuree(? Insuree $insuree): void;

    public function setContract(? Contract $contract): void;

    public function setRegistrationNumber(int $registration_number): void;
}