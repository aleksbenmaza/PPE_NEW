<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/01/2017
 * Time: 11:33
 */

/**
 * @Entity
 * @Table(name="deteriorations")
 */
class Deterioration extends Damage {
    protected
        /**
         * @ManyToOne(targetEntity="Dealer", inversedBy="deteriorations", fetch="EAGER")
         * @JoinColumn(name="garagiste_id", referencedColumnName="id", nullable=true)
         */
        $dealer = NULL,
        /**
         * @Column(name="taux", type="float")
         */
        $rate = 0.0;

    public function getDealer(): ? Dealer{
        return $this->dealer;
    }

    public function setDealer(? Dealer $dealer): void{
        $this->dealer = $dealer;
    }

    public function getRate(): float{
        return $this->rate;
    }

    public function setRate(float $rate): void{
        $this->rate = $rate;
    }


}