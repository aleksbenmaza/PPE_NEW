<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 21:51
 */

use \Doctrine\ORM\PersistentCollection;
// WARNING :
// if MappedSuperClass contains columns w/ @GeneratedValue
// and ExtendedClass contains composite key it will issue some bugs (TODO report to doctrine)

/** 
 * @MappedSuperclass
 */
abstract class Entity implements JsonSerializable {
    protected
        /**
         * @Column(name="maj", type="datetime")
         */
        $updated_at = NULL;

    public function getSID(): int{
        return static::SID;
    }

    public function updatedAt(): ? DateTime{
        return $this->updated_at;
    }
    
    public function __toString(){
        return static::class.'#'.spl_object_hash($this);
    }

    public function jsonSerialize(){
        return $this->toStdClass();
    }

    public function toStdClass(): stdClass{
        $object = new stdClass;
        foreach(get_object_vars($this) as $property => $value)
            if(is_scalar($value))
                $object->$property = $value;
        return $object;
    }

    protected static function toArraySet(PersistentCollection $container): ArraySet{
        if(!$container instanceof ArraySet)
            return new ArraySet($container->getTypeClass()->getName(), $container->toArray());
        return clone $container;
    }
}