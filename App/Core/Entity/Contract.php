<?php


/**
 * @Entity
 * @Table(name="contrats")
 */
class Contract extends Entity implements Assignable, MutableContract {
    use AssignableImpl, MutableContractImpl;
	const
		SID = 0x1C61,
        COLOR_VALID    = 0xB2FF66,
        COLOR_INVALID  = 0xFF3333,
        COLOR_AWAITING = 0xFFFF66,
        COLOR_OUTDATED = 0xC0C0C0;
	protected
        /**
         * @OneToOne(targetEntity="Vehicle", inversedBy="contract", fetch="EAGER")
         * @JoinColumn(name="vehicule_id", referencedColumnName="id")
         */
        $vehicle,
		/**
		 * @ManyToOne(targetEntity="Insurance", inversedBy="contracts", fetch="EAGER")
		 * @JoinColumn(name="type_garantie_id", referencedColumnName="id")
		 */
		$insurance,
        /**
         * @ManyToOne(targetEntity="Customer", inversedBy="contracts", fetch="EAGER")
         * @JoinColumn(name="client_id", referencedColumnName="id")
         */
        $customer,
        /**
         * @OneToMany(targetEntity="ContractAssignment", mappedBy="contract", fetch="LAZY", cascade={"all"})
         */
        $assignments;

	public function __construct(Insurance $insurance, Vehicle $vehicle,  Customer $customer){
        $this->vehicle   = $vehicle;
	    $this->insurance = $insurance;
		$this->customer  = $customer;
        $this->vehicle->setContract($this);
		$this->insurance->addContract($this);
        $this->customer->addContract($this);
	}
}