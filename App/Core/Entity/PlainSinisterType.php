<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 11/12/2016
 * Time: 17:44
 */

/**
 * @Entity
 * @Table(name="types_sinistres_sans_tiers")
 * @Cache("READ_ONLY")
 */
class PlainSinisterType extends Entity {
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @Column(name="code")
         */
        $code,
        /**
         * @Column(name="libelle")
         */
        $title,
        /**
         * @OneToMany(targetEntity="PlainSinister", mappedBy="type", fetch="LAZY", cascade={"all"})
         */
        $plain_sinisters,
        /**
         * @ManyToMany(targetEntity="Insurance",
         *             fetch="LAZY",
         *             cascade={"ALL"})
         * @JoinTable(name="types_sinistres_types_garantie",
         *            joinColumns={@JoinColumn(name="type_sinistre_id",
         *                                     referencedColumnName="id",
         *                                     unique=true)},
         *            inverseJoinColumns={@JoinColumn(name="type_garantie_id",
         *                                            referencedColumnName="id",
         *                                            unique=true)})
         */
        $covering_insurances = NULL;

    public function __construct(){
        $this->plain_sinisters     = new ArraySet(PlainSinister::class);
        $this->covering_insurances = new ArraySet(Insurance::class); 
    }

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function getCode(): string{
        return $this->code ?? '';
    }

    public function setCode(string $code): void{
        $this->code = $code;
    }

    public function getTitle(): string{
        return $this->title ?? '';
    }

    public function setTitle(string $title): void{
        $this->title = $title;
    }

    public function getPlainSinisters(): ArraySet{
        return self::toArraySet($this->plain_sinisters);
    }

    public function getCoveringInsurances(): ArraySet{
        return self::toArraySet($this->covering_insurances);
    }
    
    public function addInsurance(Insurance $insurance): bool{
        if(!$this->covering_insurances->contains($insurance))
            return FALSE;
        $this->covering_insurances[] = $insurance;
        $insurance->addSinisterType($this);
        return TRUE;
    } 
}