<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/11/2016
 * Time: 17:32
 */

/**
 * @Entity
 * @Table(name="accidents")
 */
class Accident extends Sinister{
    const
        SID = 0xCC1D;
    protected
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id = 0,
        /**
         * @ManyToMany(targetEntity="Accident", fetch="EAGER", cascade={"all"})
         * @JoinTable(name="accidents_accidents",
         *      joinColumns={
         *          @JoinColumn(name="accident_id_0", referencedColumnName="id")
         *      },
         *      inverseJoinColumns={
         *          @JoinColumn(name="accident_id_1", referencedColumnName="id")
         *      }
         * )
         */
        $accident,
        /**
         * @Column(name="taux_resp", type="float")
         */
        $responsibility_rate = 0.0;

    public function setInvolvedAccident(? Accident $accident): void{
        if(!$accident && isset($this->accident[0]))
            unset($this->accident[0]);
        else
            $this->accident[0] = $involvement;
    }

    public function getInvolvedAccident(): ? Accident{
        return $this->accident[0] ?? NULL;
    }

    public function setResponsibilityRate(float $responsibility_rate): void{
        $this->responsibility_rate = $responsibility_rate;
    }

    public function getResponsibilityRate(): float{
        return $this->responsibility_rate;
    }
}