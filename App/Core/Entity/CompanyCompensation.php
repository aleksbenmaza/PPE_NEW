<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 03/02/2017
 * Time: 18:49
 */

/**
 * @Entity
 * @Table(name="indemnisations_des_compagnies")
 */
class CompanyCompensation extends Compensation{
    protected
        /**
         * @ManyToOne(targetEntity="Company",
         *            inversedBy="compensations",
         *            fetch="LAZY")
         * @JoinColumn(name="compagnie_id", referencedColumnName="id")
         */
        $compensable;

    public function __construct(Company $company){
        $this->compensable = $company;
    }

    public function getCompany(): Company{
        return $this->compensable;
    }
}