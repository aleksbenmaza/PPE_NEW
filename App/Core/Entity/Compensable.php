<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 02/02/2017
 * Time: 12:48
 */
interface Compensable{

    public function addCompensation(Compensation $indemnity): bool;

    public function removeCompensation(Compensation $indemnity): bool;

    public function getCompensations(): ArraySet;
}