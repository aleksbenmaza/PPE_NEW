<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 16:34
 */



/**
 * @Entity
 * @Table(name="clients")
 */
class Customer extends Insuree implements RegisteredUser,
                                          Compensable,
                                          Assignable,
                                          Hydratable{
    use RegisteredUserImpl,
        CompensableImpl,
        AssignableImpl;
    const
        SID = 0x2F47;
    protected 
        /**
         * @Column(name="carte_identite")
         */
        $id_card = '',
        /**
         * @Column(name="sepa")
         */
        $sepa = '',
        /**
         * @OneToMany(targetEntity="Contract", mappedBy="customer", fetch="LAZY", cascade={"ALL"})
         */
        $contracts,
        /**
         * @OneToMany(targetEntity="CustomerAssignment", mappedBy="customer", fetch="LAZY", cascade={"all"})
         */
        $assignments,
        /**
         * @OneToMany(targetEntity="CustomerCompensation", mappedBy="compensable", fetch="LAZY", cascade={"ALL"})
         */
        $compensations;
    
    public function __construct(){
        parent::__construct();
        $this->compensations = new ArraySet(Compensation::class);
        $this->contracts     = new ArraySet(Contract::class);
        $this->assignments   = new ArraySet(Assignment::class);
    }

    public function setIdCard(string $id_card): void{
        $this->id_card = $id_card;
    }

    public function getIdCard(): string{
        return $this->id_card ?? '';
    }
    
    public function setSepa(string $sepa): void{
        $this->sepa = $sepa;
    }
    
    public function getSepa(): string{
        return $this->sepa ?? '';
    }

    public function addContract(Contract $contract): bool{
        return $this->contracts->add($contract);
    }

    public function getContracts(): ArraySet{
        return self::toArraySet($this->contracts);
    }

    public function hydrate(Hydratable $data): void{
        assert($data instanceof Customer && $data->getId() == $this->getId());
        foreach(get_object_vars($data) as $property => $value)
            if($property != 'notifs' && $value != $this->$property)
                $this->$property = $value;
    }
}