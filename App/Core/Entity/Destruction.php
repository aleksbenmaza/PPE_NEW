<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 26/01/2017
 * Time: 14:46
 */

/**
 * @Entity
 * @Table(name="destructions")
 */
class Destruction extends Damage{
    protected
        /**
         * @Column(name="est_total", type="boolean")
         */
        $entire = FALSE;

    public function isEntire(): bool{
        return $this->entire;
    }

    public function setEntire(bool $entire = TRUE): void{
        $this->entire = $entire;
    }
}