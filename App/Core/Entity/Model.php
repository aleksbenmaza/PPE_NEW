<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/10/2016
 * Time: 18:05
 */

/**
 * @Entity
 * @Table(name="modeles")
 * @Cache("READ_ONLY")
 */
class Model extends Entity{
    const
        SID = 0x10C7;
    protected
        /**
         * @ManyToOne(targetEntity="Make", inversedBy="models", fetch="EAGER")
         * @JoinColumn(name="marque_id", referencedColumnName="id")
         * @Cache("READ_ONLY")
         */
        $make,
        /**
         * @OneToMany(targetEntity="Vehicle", mappedBy="model", fetch="LAZY")
         */
        $vehicles,
        /**
         * @Id @Column(type="integer") @GeneratedValue
         */
        $id,
        /**
         * @Column(name="nom")
         */
        $name,
        /**
         * @Column(name="annee", type="integer")
         */
        $year;
    
    public function __construct(Make $make){
        $this->make     = $make;
        $this->vehicles = new ArraySet(Vehicle::class);
    }

    public function getMake(): Make{
        return $this->make;
    }

    public function getVehicles(): ArraySet{
        return clone $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): bool{
        return $this->add($vehicle);
    }
    
    public function removeVehicle(Vehicle $vehicle): bool{
        return $this->vehicles->removeElement($vehicle);
    }

    public function getId(): int{
        return $this->id ?? 0;
    }

    public function setName(string $name): void{
        $this->name = $name;
    }

    public function getName(): string{
        return $this->name ?? '';
    }

    public function setYear(int $year): void{
        $this->year = $year;
    }

    public function getYear(): int{
        return $this->year ?? 0;
    }
}