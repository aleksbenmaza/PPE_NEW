<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 14/10/2016
 * Time: 14:49
 */
class Mail{
    const
        CONFIRM = 0;
    private
        $template,
        $additional_header,
        $to,
        $header,
        $subject,
        $data = [];

    public function __construct(string $to, string $header,
                                string $subject='', string $template ='',
                                int $type = self::CONFIRM){
        $this->template = path($template);
        $this->additional_header=($type==self::CONFIRM) ?
            PHP_EOL.'Content-Type: text/plain; charset=\"iso-8859-1\"'
            :'';
        $this->to = $to;
        $this->header = $header;
        $this->subject = $subject;
    }

    public function set(string $key, $data): Mail{
        $this->data[$key]=$data;
        return $this;
    }

    public function send(): bool{
        $content = '';
        if($this->template) {
            ob_start();
            extract($this->data);
            require_once $this->template;
            $content = ob_get_clean();
        }
        return mail($this->to, $this->subject,
                    $content, $this->header,
                    $this->additional_header);
    }

}