<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 07/10/2016
 * Time: 20:10
 */
class Log{
    private function __construct(){}

    public static function write(string $event): bool{
        $f=fopen(DOCUMENT_ROOT.WEBROOT."Log/".\date("y-m-d",time()).".txt","a+");
        if(!$f)
            return FALSE;

        if(!fwrite($f,\date("h:m:s",time()).' - '.IP_CLIENT.':'.PHP_EOL.$event.PHP_EOL.PHP_EOL))
            return FALSE;
        fclose($f);
        return TRUE;
    }
}