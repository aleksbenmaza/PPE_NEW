<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 12/01/2017
 * Time: 14:36
 */
use \Doctrine\Common\Cache\CacheProvider;

class RouteFactory implements Singleton{
    private static
        $cache       = NULL,
        $initialized = FALSE,
        $got         = FALSE;
    private
        $routes      = NULL;

    public static function init(CacheProvider $cache): void{
        if(self::$initialized)
            throw new Exception(self::class.' can only be initialized once !');
        self::$cache = $cache;
        self::$initialized = TRUE;
    }

    public static function getInstance(): Singleton{
        if(!self::$initialized)
            throw new Exception(self::class.' has not been initialized !');
        if(self::$got)
            throw new Exception(self::class.' can only be got once !');
        return new RouteFactory;
    }

    public function __clone(){
        throw new RuntimeException;
    }

    public function getRoutes(): ArraySet{
        return $this->routes;
    }

    private function __construct(){
        $this->routes = new ArraySet(Route::class);
        $this->loadControllers();
        if(self::$cache->contains('routes')) {
            $this->routes = self::$cache->fetch('routes');
        }else{
            $this->parseMetadata();
            self::$cache->save('routes', $this->routes);
        }
    }

    private function loadControllers(): void{
        foreach (new DirectoryIterator(ROOT . '/App/Core/Controller') as $file) {
            if ($file->isDot() || strpos($file->getFilename(), '.') == 0)
                continue;
            require_once $file->getPathname();
        }
    }

    private function parseMetadata(): void{
        foreach (get_declared_classes() as $class) {
            $reflection_class = new ReflectionClass($class);
            if (!$reflection_class->isAbstract() && $reflection_class->isSubclassOf(Controller::class)) {
                $methods = $reflection_class->getMethods();

                foreach ($methods as $method) {
                    $doc_comment = $method->getDocComment();
                    if (!($doc_comment = trim($doc_comment)))
                        continue;
                    $doc_comment = trim($doc_comment, '/');
                    $doc_comment = str_replace('**', '', $doc_comment);
                    $doc_comment = str_replace('*', ';', $doc_comment);
                    $doc_comment = trim($doc_comment);
                    $doc_comment = trim($doc_comment, ';');
                    $doc_comment = trim($doc_comment);
                    $annotations = explode(';', $doc_comment);
                    $route = NULL;
                    foreach ($annotations as $i => $annotation) {
                        $annotation = trim($annotation);
                        if (preg_match_all('/^(?P<annotation>@[\s\S]+)$/', $doc_comment, $_)) {
                            if ($annotation[0] != '@')
                                continue;
                            $annotation = trim($annotation, '@');
                            preg_match_all('/\w\(([\s\S]*)\)/', $annotation, $matches);
                            $values = $matches[1][0] ?? '';
                            if($values)
                                $annotation = str_replace('('.$values.')', '', $annotation);

                            switch ($annotation) {
                                case 'Action':
                                    $route = new Route;
                                    $route->setAction($method->getName());
                                    $route->setController($reflection_class->getName());
                                    $this->routes[] = $route;
                                    break;

                                case 'Route':
                                    if(!$route)
                                        break;
                                    $fields = explode(',', $values, 2);
                                    foreach($fields as $field){
                                        list($name, $value) = explode('=', $field);
                                        $name  = trim($name);
                                        $value = trim($value);
                                        switch($name){
                                            case 'pattern':
                                                $route->setPattern(trim($value, '"'));
                                                break;

                                            case 'methods':
                                                $values = trim($value, '{}');
                                                $values = explode(',', $values);
                                                array_walk($values, function(string & $value): void{
                                                    $value = trim($value, '"');
                                                });
                                                if(in_array(Route::POST, $values)){
                                                    $request_param = $method->getParameters()[0] ?? NULL;
                                                    if (!$request_param
                                                    XOR !$request_param->getClass()->isSubclassOf(PostRequest::class))
                                                        throw new RuntimeException(
                                                            sprintf(
                                                                '%s action expects its signature to have a subclass of %s as 1st parameter',
                                                                $route->getAction(),
                                                                PostRequest::class
                                                            )
                                                        );
                                                    $route->setMethods($values);
                                                    $route->setPostRequest($request_param->getClass()->getName());
                                                }
                                            break;

                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}