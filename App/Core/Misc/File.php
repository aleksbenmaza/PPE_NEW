<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 05/01/2017
 * Time: 16:12
 */

class File{

    private function __construct(){}

    public static function save(string $old_path, string $ext): string{
        $old_name = basename($old_path);
        $set = array_merge(range('0','9'), range('A','Z'), range('a','z'));
        $len = strlen($old_name);
        do{
            $set = array_rand($set, $len);
            $new_name = implode($set);
            $new_path = STORAGE_ROOT.$new_name.'.'.$ext;
        }while(file_exists($new_path));

        return (rename($old_path, $new_path)) ? $new_path : '';
    }
}