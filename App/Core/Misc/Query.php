<?php

/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 21/10/2016
 * Time: 15:30
 */
final class Query{
    private
        $selected_fields = [],
        $table           = '',
        $inner_joined    = [],
        $left_joined     = [],
        $right_joined    = [],
        $where_searchs   = [],
        $grouped_by      = [],
        $having_searchs  = [],
        $ordered_by      = [],
        $limited_by      = [];

    public function select(string... $fields): Query{
        $this->selected_fields=$fields;
        return $this;
    }

    public function from(string $table): Query{
        $this->table=$table;
        return $this;
    }

    public function innerJoin(string $table, string $left_field, string $comp_op, string $right_field): Query{

        $this->inner_joined[$table][] = $left_field;
        $this->inner_joined[$table][] = $right_field;
        $this->inner_joined[$table][] = $comp_op;
        return $this;
    }

    public function leftJoin(string $table, string $left_field, string $comp_op, string $right_field): Query{

        $this->left_joined[$table][] = $left_field ;
        $this->left_joined[$table][] = $right_field;
        $this->left_joined[$table][] = $comp_op;
        return $this;
    }

    public function rightJoin(
        string $table,
        string $left_field,
        string $comp_op,
        string $right_field
    ): Query {

        $this->right_joined[$table][] = $left_field;
        $this->right_joined[$table][] = $right_field;
        $this->right_joined[$table][] = $comp_op;
        return $this;
    }

    public function where(Search ...$searchs): Query{
        $this->where_searchs = $searchs;
        return $this;
    }

    public function groupBy(string ...$fields): Query {
        $this->grouped_by = $fields;
        return $this;
    }
    
    public function having(Search ...$searchs): Query {
        $this->having_searchs = $searchs;
        return $this;
    }
    
    public function orderBy(string ...$fields): Query {
        $this->ordered_by = $fields;
        return $this;
    }
    
    public function limit(int $min, int $max=0): Query {
        $this->limited_by[] = $min;
        if($max)
            $this->limited_by[] = $max;
        return $this;
    }
    
    public function __toString(): string {
        
        $query = 'FROM '.$this->table.' ';
        
        if($this->inner_joined)
            foreach($this->inner_joined as $table => list($left_field,$right_field,$comp_op)) {
                $query .= 'INNER JOIN ' . $table . ' ON ';
                $query .= $this->formatJoin($table, $left_field, $right_field, $comp_op).' ';

            }

        if($this->left_joined)
            foreach($this->left_joined as $table => list($left_field,$right_field,$comp_op)) {
                $query .= 'LEFT JOIN ' . $table . ' ON ';
                $query .= $this->formatJoin($table, $left_field, $right_field, $comp_op).' ';
            }

        if($this->right_joined)
            foreach($this->right_joined as $table => list($left_field,$right_field,$comp_op)) {
                $query .= 'RIGHT JOIN ' . $table . ' ON ';
                $query .= $this->formatJoin($table, $left_field, $right_field,$comp_op).' ';
            }
        
        if($this->where_searchs) {
            $query .= 'WHERE ';
            $query .= implode(' OR ', $this->where_searchs).' ';
        }

        $select = 'SELECT ';
        $select .= implode(',', $this->selected_fields).' ';
        $query = $select . $query;
        
        if($this->grouped_by){
            $query .= 'GROUP BY ';
            $query .= implode(',', $this->grouped_by).' ';
        }
        
        if($this->having_searchs){
            $query .= 'HAVING ';
            $query .= implode(' OR ', $this->grouped_by).' ';
        }
        
        if($this->ordered_by){
            $query .= 'ORDER BY ';
            $query .= implode(',', $this->ordered_by);
        }
        
        if($this->limited_by){
            $query .= 'LIMIT ';
            $query .= implode(',', $this->limited_by);
        }
        
        return $query;
    }

    private function formatJoin(string $table, string $left_field, string $right_field, string $comp_op): string{

        foreach ($this->where_searchs as $search)
            foreach ($search->getAll() as $field => $values)
                if ($right_field == $field || $left_field == $field)
                    $search->rename($field, $table . '.' . $field);

        foreach ($this->selected_fields as &$field)
            if ($right_field == $field || $left_field == $field)
                $field = $this->table . '.' . $field;

        $left_field = $this->table . '.' . $left_field;
        $right_field = $table . '.' . $right_field;

        foreach ($this->selected_fields as &$field)
            if ($right_field == $field)
                $field = $this->table . '.' . $field;

        return $left_field . $comp_op . $right_field;
    }
}