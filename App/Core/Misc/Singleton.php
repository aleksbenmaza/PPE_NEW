<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 05/01/2017
 * Time: 15:39
 */


interface Singleton{
    public static function getInstance(): Singleton;
    
    public function __clone();
}