<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 12/12/2016
 * Time: 20:47
 */



use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use \Doctrine\Common\Cache\CacheProvider;
use \Doctrine\Common\Cache\ApcuCache;
use \Doctrine\Common\Cache\FilesystemCache;
use \Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Tools;
use \Doctrine\ORM\Cache\DefaultCacheFactory;
use \Doctrine\ORM\Cache\RegionsConfiguration;

class Configuration implements Singleton {
    private const
        DATABASE_DRIVER = 'pdo_mysql',
        DATABASE_HOST = '127.0.0.1',
        DATABASE_PORT = 81,
        DATABASE_USER = '***',
        DATABASE_PWD = '***',
        DATABASE_NAME = '***';
    private static
        $got = FALSE,
        $sqlLogger = NULL;
    private
        $entity_manager = NULL,
        $cache = NULL;
    
    public static function getInstance(): Singleton{
        if(self::$got)
            throw new RuntimeException(self::class . ' can only be got once.');
        self::$got = TRUE;
        return new Configuration;
    }

    public static function getSQLLogger(): ? Doctrine\DBAL\Logging\EchoSQLLogger{
        return self::$sqlLogger;
    }

    private function __construct(){ 
        $paths = [DOCUMENT_ROOT.'/.doctrine/cache'];
        $isDevMode = FALSE;
        $dbParams = [
            'driver'   => self::DATABASE_DRIVER,
            'host'     => self::DATABASE_HOST,
            'user'     => self::DATABASE_USER,
            'password' => self::DATABASE_PWD,
            'dbname'   => self::DATABASE_NAME,
        ];

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        if(extension_loaded('apcu'))
            $this->cache = new ApcuCache;
        else
            try{
                $this->cache = new FilesystemCache($paths[0]);
            }catch(Exception $exception){
                Log::write($exception->getMessage());
                $this->cache = new ArrayCache;
            }
        $regionConfig = new RegionsConfiguration;
        $factory = new DefaultCacheFactory($regionConfig, $this->cache);
        $config->setSecondLevelCacheEnabled();
        $config->getSecondLevelCacheConfiguration()->setCacheFactory($factory);
        $config->setQueryCacheImpl($this->cache);
        $config->setResultCacheImpl($this->cache);
        $config->setMetadataDriverImpl($config->newDefaultAnnotationDriver(DOCUMENT_ROOT.'/App/Core/Entity'));
        $config->setMetadataCacheImpl($this->cache);
        $config->setProxyDir(DOCUMENT_ROOT.'/.doctrine/proxy');
        $config->setAutoGenerateProxyClasses(FALSE);
        $config->setSQLLogger(self::$sqlLogger = new \Doctrine\DBAL\Logging\LoggerChain());
        $this->entity_manager = EntityManager::create($dbParams, $config);
    }

    public function __destruct(){
        $this->entity_manager->flush();
    }

    public function __clone(){
        throw new RuntimeException(self::class.' is not clonable !');
    }

    public function getEntityManager(): EntityManager{
        return $this->entity_manager;
    }

    public function getCache(): CacheProvider{
        return $this->cache;
    }
}