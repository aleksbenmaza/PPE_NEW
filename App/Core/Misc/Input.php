<?php

final class Input implements Singleton{
	private 
		$data = [],
		$trash = [];
	private static
		$got = FALSE;

	public static function getInstance(): Singleton{
		if(self::$got)
			throw new RuntimeException(self::class.' can only be got once.');
		self::$got = TRUE;
		return new Input;
	}

	private function __construct(){
		if(!(isset($_POST) || isset($_FILES)))
			return;
		else if($_POST && $_FILES)
			$data = array_merge($_POST,$_FILES);
		else if($_POST && !$_FILES)
			$data = $_POST;
		else
			$data = $_FILES;

		foreach($data as $key => $value)
			if(is_array($value))
				$this->data[$key] = $value;
			else if(!is_array($value) && strpbrk($value,'=/\/*_;()&#+?%\'')===FALSE)
				$this->data[$key] = $value;
			else
				$this->trash[$key] = $value;
		unset($_POST,$_FILES);
	}

	public function __clone(){
		throw new RuntimeException(self::class.' is not clonable !');
	}

	public function get(string $key) {
		return $this->data[$key] ?? NULL;
	}
	
	public function getAll(): array{
		return $this->data;
	}

	public function flushTrash(): array{
		$trash = $this->trash;
		$this->trash = [];
		return $trash;
	}
}