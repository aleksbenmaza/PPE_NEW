<?php
/**
 * Created by PhpStorm.
 * User: alexandremasanes
 * Date: 16/01/2017
 * Time: 11:37
 */


class FormField{
    private
        $id_name = '',
        $type = '',
        $default_value = '',
        $class = '',
        $read_only = FALSE,
        $place_holder = '';

    public function getIdName(): string{
        return $this->id_name;
    }

    /**
     * @param string $id_name
     * @return FormField
     */
    public function setIdName(string $id_name): FormField{
        $this->id_name = $id_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string{
        return $this->type;
    }

    /**
     * @param string $type
     * @return FormField
     */
    public function setType(string $type): FormField{
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultValue(): string{
        return $this->default_value;
    }

    /**
     * @param string $default_value
     * @return FormField
     */
    public function setDefaultValue(string $default_value): FormField{
        $this->default_value = $default_value;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string{
        return $this->class;
    }

    /**
     * @param string $class
     * @return FormField
     */
    public function setClass(string $class): FormField{
        $this->class = $class;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReadOnly(): bool{
        return $this->read_only;
    }

    /**
     * @param bool $read_only
     * @return FormField
     */
    public function setReadOnly(bool $read_only = TRUE): FormField{
        $this->read_only = $read_only;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceHolder(): string{
        return $this->place_holder;
    }

    /**
     * @param string $place_holder
     * @return FormField
     */
    public function setPlaceHolder(string $place_holder): FormField{
        $this->place_holder = $place_holder;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string{
        return '<input type="'.$this->type.'"
                       name="'.$this->id_name.'"
                       id="'.$this->id_name.'"
                       class="'.$this->class.'" 
                       value="'.$this->default_value.'"
                       '.($this->read_only) ? 'readonly' : ''.'>';
    }
}