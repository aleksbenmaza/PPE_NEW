<?php require_once path('header') ?>
<?php require_once path('navbar') ?>
<?php require_once path('banner') ?>
    <div id="container">
        <div id="banner">
                <div class="image-border">
                    <a href="#">
                        <img src="<?=WEBROOT?>Public/Images/Depositphotos_1981846_original.jpg" width="870" height="253" alt="" class="img-responsive" />
                    </a>
                </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" style = " background: #353535; color: #AFAFAF;">
                <h3 class="panel-title">
                    Outil de calcul de franchise
                </h3>
            </div>
            <div class="panel-body">
                <p>
                    Intéressé par notre assurance ? Déjà assuré ?
                </p>
                <p>
                    Vous vous préocuppez sûrement de la franchise à payer en cas de sinistre. Voici pour vous un utilitaire de calcul de franchise en fonction des réparations :
                </p>
                <div class="col-md-4">
                    <label>
                        Le type de garantie :
                    </label>
                    <SELECT class="form-control" id="insurances" name="insurances"  >
                        <OPTION value=" ">
                    </SELECT>
                    <br/>
                    <label>
                        Police annuelle de base :
                    </label>
                    <div class="input-group">
                        <input class="form-control" id="default_amount" readonly>
                        <span class="input-group-addon">€</span>
                    </div>
                    <br/>
                    <label>
                        Le montant de vos réparations :
                    </label>
                    <div class="input-group">
                        <input class="form-control" type="text" id="amount">
                        <span class="input-group-addon">€</span>
                    </div>
                    <br/>
                    <label>
                        La franchise à payer :
                    </label>
                    <div class="input-group">
                        <input class="form-control" type="text" id="deductible" value=" " readonly>
                        <span class="input-group-addon">€</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once path('footer') ?>