<?php require_once path('header') ?>
    <div>
        <form method="POST" action="<?=WEBROOT?>inscription-p/" class="sign" enctype="multipart/form-data">
            <label for="nom">Nom</label><br/>
            <input type="text" name="name" id="name" size="30" value="<?=$last_name??''?>" /><label class="errors"><?=$error_last_name??''?></label><br/>
            <label for="prenom">Prenom</label><br />
            <input type="text" name="first_name" id="first_name" size="30" value="<?=$first_name??''?>" /><label class="errors"><?=$error_first_name??''?></label><br />

            <label for="mdp">Mot de passe</label><br />
            <input type="password" name="pwd" id="pwd" size="30"  value="<?=$password??''?>" /><label class="errors"><?=$errorr_password??''?></label><br />
            <label for="mdp2">Confirmez votre mot de passe</label><br />
            <input type="password" name="pwd2" id="pwd2" size="30"  value="<?=$password_confirm??''?>" /><label class="errors"><?=$error_password_confirm??''?></label><br />
            <label for="adresse">Adresse </label><br />
            <input type="text" name="address" id="address" size="30" value="<?=$adress??''?>" /><label class="errors"><?=$error_address??''?></label><br />
            <label for="ville">Ville </label><br />
            <input type="text" name="city" id="city" size="30" value="<?=$city??''?>" /><label class="errors"><?=$error_cicty??''?></label><br />
            <label for="code_postal">Code Postal </label><br />
            <input type="text" name="zip_code" id="zip_code" size="30" value="<?=$zip_code??''?>" /><label class="errors"><?=$error_zip_code??''?></label><br />

            <label for="tel">Telephone </label><br />
            <input type="text" name="phone_number" id="phone_number" size="30" value="<?=$zip_code??''?>" /><label class="errors"><?=$error_phone_number??''?></label><br />
            <label for="mail">Adresse email</label><br />
            <input type="email" name="email" id="email" size="30"  value="<?=$email??''?>" /><label class="errors"><?=$error_email??''?></label><br />
            <label for="email2">Confirmez votre adresse email</label><br />
            <input type="email" name="email2" id="email2" size="30"  value="<?=$email2??''?>" /><label class="errors"><?=$error_email??''?></label><br />
            <label for="permis">Pièce d'identité </label><br />
            <input type="file" name="id_card" id="id_card"  style="position: relative; margin-left: 42.5%"  /><label class="errors"><?=$error_id_card??''?></label><br /><br /><br/>
            <input type="submit" name="submit" value="Valider" class="valider" />
        </form>
    </div>
<?php require_once path('footer') ?>
