<?php require_once path('header') ?>
<?php require_once path('navbar') ?>
<?php require_once path('banner') ?>
    <div id="container">
        <div id="banner">
            <div class="image-border"><a href="#"><img src="<?=WEBROOT?>Public/Images/aix-en-provence-161731.jpg" width="870" height="253" alt="" class="img-responsive" /></a></div>
        </div>
        <div class="col-md-offset-2 same-line">
            <div class="panel panel-default col-md-4">
                <div class="panel-heading " style = " background: #353535; color: #AFAFAF;">
                    <h3 class="panel-title">
                        Présentation
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        L’Assurance Automobile Aixoise est l’agence d’une grande compagnie internationale d’assurances. Elle emploie soixante personnes et couvre les activités d’assurance incendie, automobile, vie, décès et divers.
                    </p>
                </div>
            </div>
            <div class="panel panel-default col-md-4">
                <div class="panel-heading" style = " background: #353535; color: #AFAFAF;">
                    <h3 class="panel-title">
                        Raison sociale
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        Assurance Automobile Aixoise
                    </p>
                    <p>
                        SA au capital de 100 000 €
                    <p>
                    <p>
                        2 place de la Sainte-Victoire
                        13100 Aix-en-Provence Cedex.
                    </p>
                    <p>
                        &nbsp;
                    </p>
                </div>
            </div>
            <div class="panel panel-default col-md-4">
                <div class="panel-heading" style = " background: #353535; color: #AFAFAF;">
                    <h3 class="panel-title">
                        Contactez-nous
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        Par courriel :
                    </p>
                    <p>
                        contact@aaa.com
                    <p>
                    <p>
                        Par téléphone :
                    </p>
                    <p>
                        0512987xxx
                    </p>
                    <p>
                        &nbsp;
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php require_once path('footer') ?>
