<h1><?=$subject??''?></h1>
<div>
    <p>
        Bienvenue sur le site de l'Assurance Automobile Aixoise,

        Pour valider votre adresse email, veuillez cliquer sur le lien ci dessous
        ou copier/coller dans votre navigateur internet.
    </p>
    <?php if(isset($link)): ?>
        <a href="<?=$link->url??''?>"><?=$link->title??''?></a>
    <?php endif ?>
    <p>
        ---------------
        Ceci est un mail automatique, merci de ne pas y répondre.
    </p>
</div>