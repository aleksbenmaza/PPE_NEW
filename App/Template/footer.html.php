		<div id="footer">
			<p>
				&copy; 
				<a href="<?=WEBROOT?>">
					Assurance Automobile Aixoise
				</a>
				&nbsp;&nbsp;|&nbsp;&nbsp; 
				by 
				<a href="">
					SysLog
				</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
			</p>
		</div>
		<?php if(isset($js_vars)): ?>
			<script type="text/javascript">

			</script>
		<?php endif ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<?php if(isset($js_constants)): ?>
			<script type="text/javascript">
				<?php foreach($js_constants as $const => $value): ?>
					const <?=$const?> = <?=$value?> ;
				<?php endforeach ?>
			</script>
		<?php endif ?>
		<?php if(isset($notifs)): ?>
			<script type="text/javascript">
				var notifs = <?=View::toJson($notifs)?>;
			</script>
			<script  type="text/javascript" src="<?=WEBROOT?>Public/JavaScript/common-app.js"></script>
		<?php endif ?>
		<?php if(isset($scripts)): ?>
			<?php foreach($scripts as $script):?>
				<script src="<?=WEBROOT.$script?>"></script>
			<?php endforeach?>
		<?php endif ?>
	</body>
</html>

	
